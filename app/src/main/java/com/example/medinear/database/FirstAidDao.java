package com.example.medinear.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.medinear.model.Accident;
import com.example.medinear.model.Clinic;

import java.util.List;

@Dao
public interface FirstAidDao {

    @Insert
    void insert(Accident accident);

    @Query("SELECT * FROM accident_table ORDER BY name ASC")
    LiveData<List<Accident>> getFirstAidGuides();

    @Query("SELECT * FROM accident_table WHERE name LIKE '%' || :guide || '%' OR description LIKE '%' || :guide || '%' " +
            "OR guidelines LIKE '%' || :guide || '%' OR tags LIKE '%' || :guide || '%'")
    LiveData<List<Accident>> searchGuides(String guide);
}
