package com.example.medinear.database;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.example.medinear.model.Clinic;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {Clinic.class}, version = 1, exportSchema = false)
public abstract class ClinicRoomDatabase extends RoomDatabase {
    public abstract ClinicDao clinicDao();
    private static volatile ClinicRoomDatabase INSTANCE;
    private static final int NUMBER_OF_THREADS = 4;
    public static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    public static ClinicRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (ClinicRoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            ClinicRoomDatabase.class, "clinic_database").allowMainThreadQueries().build();
                }
            }
        }
        return INSTANCE;
    }



    private static RoomDatabase.Callback sRoomDatabaseCallback = new RoomDatabase.Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            databaseWriteExecutor.execute(new Runnable() {
                @Override
                public void run() {
                    //Pre-populate the data in the background
                    ClinicDao dao = INSTANCE.clinicDao();
//                        dao.deleteAll();

//                    dao.insert(new Clinic("Thu Cuc hospitalz", "So 32, Ly Name De street, Hanoi, Vietnam",
//                            "https://ngaymoionline.com.vn/media/uploaded/48/2019/08/28/bv_thucuc.jpg",
//                            3.8F, 50, 50, 21.0143915, 105.7993481, "0396149496", "General"));
//                    dao.insert(new Clinic("Quan Y hospital",
//                            "157 Nguyen Truong To street, Hanoi, Vietnam",
//                            "https://youmed.vn/tin-tuc/wp-content/uploads/2019/06/benh-vien-quan-y-7a-3-1.jpg",
//                            4.1F, 50, 50, 21.0180285, 105.7595020, "0396149496", "General"));
//                    dao.insert(new Clinic("Viet Hung Dental clinic",
//                            "K19, Van Hanh street Viet Hung urban area, Vietnam",
//                            "https://static.dentaldepartures.com/clinics/dd_201604030340_54370506c1cb4.jpg",
//                            1.4F, 50, 50, 21.05152095, 105.79281541, "0396149496", "Dentist"));
//                    dao.insert(new Clinic("Hanoi Lung hospital",
//                            "417 Hoang Hoa Tham street, Hanoi, Vietnam",
//                            "https://thuocdantoc.vn/wp-content/uploads/2019/07/benh-vien-phoi-ha-noi.jpg",
//                            2.1F, 50, 50, 21.0716320, 105.7950423, "0396149496", "Lung"));
//                    dao.insert(new Clinic("Ho Chi Minh Heart hospital",
//                            "34 Nguyen Thi Thap, District 3, Ho Chi Minh city, Vietnam",
//                            "https://cdn.bookingcare.vn/fo/2019/01/28/233014benh-vien-tim-tam-duc.png",
//                            2.5F, 50, 50, 21.0177293, 105.7983714, "0396149496", "Heart"));
//                    dao.insert(new Clinic("Grade A clinic",
//                            "99 Random street, Singapore",
//                            "https://www.dsc-clinic.sg/PublishingImages/_NSC1475medit.jpg",
//                            5.0F, 50, 50, 21.0314012, 105.7393163, "0396149496", "General"));

                }
            });
        }
    };


}
