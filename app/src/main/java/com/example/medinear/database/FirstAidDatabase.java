package com.example.medinear.database;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.DatabaseConfiguration;
import androidx.room.InvalidationTracker;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.sqlite.db.SupportSQLiteOpenHelper;

import com.example.medinear.R;
import com.example.medinear.model.Accident;
import com.example.medinear.model.Clinic;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {Accident.class}, version = 1, exportSchema = false)
public abstract class FirstAidDatabase extends RoomDatabase {
    public abstract FirstAidDao firstAidDao();
    private static FirstAidDatabase INSTANCE;

    private static final int NUMBER_OF_THREADS = 4;
    public static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    public static FirstAidDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (FirstAidDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            FirstAidDatabase.class,"first_aid_database")
                            .fallbackToDestructiveMigration().addCallback(sRoomDatabaseCallback)
                            .build();
                }
            }
        }

        return INSTANCE;
    }

    private static RoomDatabase.Callback sRoomDatabaseCallback = new RoomDatabase.Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            databaseWriteExecutor.execute(new Runnable() {
                @Override
                public void run() {
                    //Pre-populate the data in the background
                    FirstAidDao dao = INSTANCE.firstAidDao();
                    dao.insert(new Accident("Anaphylaxis","A serious allergic reaction that is rapid in onset and may cause death.",
                            "https://image.flaticon.com/icons/png/512/2855/2855049.png", R.string.anaphylaxis ,
                            "itchy rash, throat or tongue swelling, shortness of breath, vomiting, lightheadedness, low blood pressure, and temporary blindness"));
                    dao.insert(new Accident("Bleeding","Blood escaping from the circulatory system from damaged blood vessels.",
                            "https://image.flaticon.com/icons/png/512/883/883986.png",R.string.bleeding,
                            "bleed, cut, knife, blood, injury"));
                    dao.insert(new Accident("Burn or scald","Damage to the skin caused by heat. Both are treated in the same way.",
                            "https://image.flaticon.com/icons/png/512/2979/2979348.png",R.string.burn_or_scald,
                            "hot water, steam, dry heat, iron, fire, hot"));
                    dao.insert(new Accident("Electric shock","A physiological reaction caused by electric current passing through the body.",
                            "https://image.flaticon.com/icons/png/512/2997/2997217.png",R.string.electric_shock,
                            "electricity, shock, plug, switches, lightning, storm, electric"));
                    dao.insert(new Accident("Choking","Choking occurs when breathing is impeded by a constricted or obstructed throat or windpipe.",
                            "https://image.flaticon.com/icons/png/512/2231/2231110.png",R.string.choking,
                            "can't breathe, food choke, stuck, airway blocked"));
                    dao.insert(new Accident("Fracture","A partial or complete break in the continuity of the bone.",
                            "https://image.flaticon.com/icons/png/512/2932/2932524.png",R.string.fracture,
                            "accident, car accident, fall, break, hit, crash"));
                    dao.insert(new Accident("Food poisoning","An illness caused by eating contaminated food.",
                            "https://image.flaticon.com/icons/png/512/921/921807.png",R.string.food_poisoning,
                            "bad, stale, raw, nausea, vomiting, diarrhoea, chills, aching muscles, loss of appetite, stomach cramps, abdominal pain, virus, Escherichia, virus, Ecoli"));
                    dao.insert(new Accident("Drowning","Respiratory impairment as a result of being in or under a liquid.",
                            "https://image.flaticon.com/icons/png/512/2983/2983928.png",R.string.drowning,
                            "drown, water, flood, pool, swimming, swim"));
                    dao.insert(new Accident("Heart attack","Blood flow decreases or stops to a part of the heart, causing damage to the heart muscle.",
                            "https://image.flaticon.com/icons/png/512/3030/3030806.png",R.string.heart_attack,
                            "nausea, cold sweat, lightheadedness, dizziness, shortness of breath, pressure, tightness, pain, squeezing or aching sensation"));
                    dao.insert(new Accident("Stroke", "A life-threatening situation when the blood supply to part of the brain is cut off.",
                            "https://image.flaticon.com/icons/png/512/2888/2888816.png",R.string.stroke,
                            "face drop to a side, cannot, unable to lift both arms, speech slurred or garbled, not be able to speak"));
                }
            });
        }
    };
}
