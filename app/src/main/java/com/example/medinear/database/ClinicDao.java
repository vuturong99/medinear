package com.example.medinear.database;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.medinear.model.Clinic;

import java.util.List;

@Dao
public interface ClinicDao {

    //Declare Queries here
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Clinic clinic);

//    @Query("DELETE FROM clinics_table")
//    void deleteAll();

    @Query("SELECT * FROM clinics_table ORDER BY name ASC")
    List<Clinic> getAlphabetizeWords();

    @Query("SELECT * FROM clinics_table")
    LiveData<List<Clinic>> getAllClinics();

    @Query("SELECT * FROM clinics_table")
    List<Clinic> getClinics();

    @Query("SELECT * FROM clinics_table WHERE type = :type")
    LiveData<List<Clinic>> getClinicsByType(String type);

    @Query("SELECT * FROM clinics_table WHERE id = :id")
    Clinic getClinicByID(int id);

    @Query("SELECT * FROM clinics_table WHERE name LIKE '%' || :name || '%' OR address LIKE '%' || :name || '%' ")
    LiveData<List<Clinic>> searchClinics(String name);

    @Query("SELECT * FROM clinics_table ORDER BY rating DESC")
    List<Clinic> sortByRatings();

    @Query("SELECT * FROM clinics_table ORDER BY name DESC")
    List<Clinic> sortByAlphaDesc();


    @Query("UPDATE clinics_table SET rating = :rate WHERE id = :id")
    void updateRating(double rate, int id);

    @Query("UPDATE clinics_table SET imageURL = :imageURL WHERE id = :id")
    void updateImageURL(int id, String imageURL);

    @Query("UPDATE clinics_table " +
            "SET monday_time = :monday_time, " +
            "tuesday_time = :tuesday_time, " +
            "wednesday_time = :wednesday_time," +
            "thursday_time = :thursday_time," +
            "friday_time = :friday_time," +
            "saturday_time = :saturday_time, " +
            "sunday_time = :sunday_time WHERE id = :id")
    void updateOpeningHours(String monday_time, String tuesday_time, String wednesday_time, String thursday_time,
                            String friday_time, String saturday_time, String sunday_time, int id);

    @Query("UPDATE clinics_table " +
            "SET name = :name," +
            "lat = :lat," +
            "long = :lng," +
            "address = :address," +
            "imageURL = :imageURL," +
            "phone = :phone," +
            "type = :type," +
            "rating = :rating," +
            "eng_name = :eng_name," +
            "type = :type WHERE id = :id")
    void updateHospital(String name, double lat, double lng, String address, String imageURL, String phone,
                        String type, double rating, String eng_name, int id);

}
