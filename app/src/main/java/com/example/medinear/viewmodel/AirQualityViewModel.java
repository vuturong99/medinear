package com.example.medinear.viewmodel;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class AirQualityViewModel extends ViewModel {
    MutableLiveData<Integer> aqi = new MutableLiveData<>();

    public MutableLiveData<Integer> getAqi() {
        return aqi;
    }

    public void setAqi(MutableLiveData<Integer> aqi) {
        this.aqi = aqi;
    }
}
