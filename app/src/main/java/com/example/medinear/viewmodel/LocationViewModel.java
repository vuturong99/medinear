package com.example.medinear.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.android.gms.maps.model.LatLng;

public class LocationViewModel extends ViewModel {
    private MutableLiveData<LatLng> mCurrentLocation = new MutableLiveData<>();

    public void setCurrentLocation(LatLng currentLocation) {
        mCurrentLocation.setValue(currentLocation);
    }

    public LiveData<LatLng> getCurrentLocation() {
        return mCurrentLocation;
    }
}
