package com.example.medinear.viewmodel;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.medinear.model.Clinic;
import com.example.medinear.repository.ClinicRepository;

import java.util.List;

public class ClinicViewModel extends AndroidViewModel {
    private ClinicRepository mRepository;


    private LiveData<List<Clinic>> allClinics;
    private MutableLiveData<List<Clinic>> mAllClinics = new MutableLiveData<>();
    private List<Clinic> mAlphabetizedClinics;
    private List<Clinic> mReAlphabetizedClinics;
    private List<Clinic> mHighRatingsClinics;

    public ClinicViewModel (Application application) {
        super(application);
        mRepository = new ClinicRepository(application);
        allClinics = mRepository.getAllClinics();
        mAlphabetizedClinics = mRepository.getAlphabetizedClinics();
        mReAlphabetizedClinics = mRepository.getReAlphabetizedClinics();
        mHighRatingsClinics = mRepository.getHighRatingsClinics();
    }

    //All clinics but not mutable
    public LiveData<List<Clinic>> getAllClinics() { return allClinics; }

    //All clinics but mutable
    public LiveData<List<Clinic>> getMutableClinics() { return mAllClinics; }

    public LiveData<List<Clinic>> getClinicsByType(String type) {
        return mRepository.getClinicsByType(type);
    }


    //Return clinics sorted alphabetically
    public List<Clinic> getAlphabetizedClinics() {
        return mAlphabetizedClinics;
    }

    //Return clinics sorted reversed alphabetically
    public List<Clinic> getReAlphabetizedClinics() {
        return mReAlphabetizedClinics;
    }

    //Return clinics sorted by ratings
    public List<Clinic> getHighRatingsClinics() {
        return mHighRatingsClinics;
    }

    public Clinic getClinicByID(int id) {
        return mRepository.getClinicByID(id);
    }

    public LiveData<List<Clinic>> searchClinics(String name) {
        return mRepository.searchClinics(name);
    }

    public void insert(Clinic clinic) { mRepository.insert(clinic); }

    public void setClinics(List<Clinic> clinics) {
        mAllClinics.setValue(clinics);
    }
}

