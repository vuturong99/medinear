package com.example.medinear.viewmodel;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.medinear.model.Accident;
import com.example.medinear.repository.ClinicRepository;
import com.example.medinear.repository.FirstAidRepository;

import java.util.List;

public class FirstAidViewModel extends AndroidViewModel {
    FirstAidRepository firstAidRepository;
    LiveData<List<Accident>> allGuides;
    LiveData<List<Accident>> searchedGudies;

    public FirstAidViewModel (Application application) {
        super(application);
        firstAidRepository = new FirstAidRepository(application);
        allGuides = firstAidRepository.getAllGuides();
    }

    public LiveData<List<Accident>> getAllGuides() {
        return allGuides;
    }

    public LiveData<List<Accident>> getSearchedGudies(String query) {
        return firstAidRepository.getSearchedGuides(query);
    }

}
