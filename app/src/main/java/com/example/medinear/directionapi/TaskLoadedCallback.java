package com.example.medinear.directionapi;

public interface TaskLoadedCallback {
    void onTaskDone(Object... values);
}
