package com.example.medinear.repository;

import android.app.Application;

import androidx.lifecycle.LiveData;

import com.example.medinear.database.FirstAidDao;
import com.example.medinear.database.FirstAidDatabase;
import com.example.medinear.model.Accident;

import java.util.List;

public class FirstAidRepository {
    private FirstAidDao firstAidDao;
    private LiveData<List<Accident>> allGuides;
    private LiveData<List<Accident>> searchGuides;

    public FirstAidRepository(Application application) {
        FirstAidDatabase firstAidDatabase = FirstAidDatabase.getDatabase(application);
        firstAidDao = firstAidDatabase.firstAidDao();
        allGuides = firstAidDao.getFirstAidGuides();

    }

    public LiveData<List<Accident>> getAllGuides() {
        return allGuides;
    }

    public LiveData<List<Accident>> getSearchedGuides(String query) {
        return firstAidDao.searchGuides(query);
    }
}
