package com.example.medinear.repository;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.medinear.database.ClinicDao;

import com.example.medinear.database.ClinicRoomDatabase;
import com.example.medinear.model.Clinic;

import java.util.List;

public class ClinicRepository {

    private ClinicDao mClinicDao;
    private LiveData<List<Clinic>> mAllClinicsById;
    private List<Clinic> mAlphabetizedClinics;
    private List<Clinic> mReAlphabetizedClinics;
    private List<Clinic> mHighRatingsClinics;

    public ClinicRepository(Application application) {
        ClinicRoomDatabase db = ClinicRoomDatabase.getDatabase(application);
        mClinicDao = db.clinicDao();
        mAlphabetizedClinics = mClinicDao.getAlphabetizeWords();
        mAllClinicsById = mClinicDao.getAllClinics();
        mReAlphabetizedClinics = mClinicDao.sortByAlphaDesc();
        mHighRatingsClinics = mClinicDao.sortByRatings();

    }

    // Room executes all queries on a separate thread.
    // Observed LiveData will notify the observer when the data has changed.
    public LiveData<List<Clinic>> getAllClinics() {
        return mAllClinicsById;
    }

    //Return clinics sorted alphabetically
    public List<Clinic> getAlphabetizedClinics() {
        return mAlphabetizedClinics;
    }

    //Return clinics sorted reversed alphabetically
    public List<Clinic> getReAlphabetizedClinics() {
        return mReAlphabetizedClinics;
    }

    //Return clinics sorted by ratings
    public List<Clinic> getHighRatingsClinics() {
        return mHighRatingsClinics;
    }
    public LiveData<List<Clinic>> getClinicsByType(String type) {
        return mClinicDao.getClinicsByType(type);
    }

//    public List<Clinic> getClinics() {
//        return mClinicDao.getClinics();
//    }

    public Clinic getClinicByID(int id) {
        return mClinicDao.getClinicByID(id);
    }

    public LiveData<List<Clinic>> searchClinics(String name) {
        return mClinicDao.searchClinics(name);
    }


//     You must call this on a non-UI thread or your app will throw an exception. Room ensures
//     that you're not doing any long running operations on the main thread, blocking the UI.
    public void insert(final Clinic clinic) {
        ClinicRoomDatabase.databaseWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                mClinicDao.insert(clinic);
            }
        });
    }
}