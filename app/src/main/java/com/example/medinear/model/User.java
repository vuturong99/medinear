package com.example.medinear.model;

public class User {
    private String real_name;
    private String email;
    private String emergency_contact;
    private String address;
    private String role;

    public User(String real_name, String email, String emergency_contact, String address, String role) {
        this.real_name = real_name;
        this.email = email;
        this.emergency_contact = emergency_contact;
        this.address = address;
        this.role = role;
    }

    public String getReal_name() {
        return real_name;
    }

    public void setReal_name(String real_name) {
        this.real_name = real_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public String getEmergency_contact() {
        return emergency_contact;
    }

    public void setEmergency_contact(String emergency_contact) {
        this.emergency_contact = emergency_contact;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
