package com.example.medinear.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "accident_table")
public class Accident implements Parcelable {


    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int ID;

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "description")
    private String description;

    @ColumnInfo(name = "imageURL")
    private String imageURL;

    @ColumnInfo(name = "guidelines")
    private int guidelines;

    @ColumnInfo(name = "tags")
    private String tags;

    public Accident(String name, String description, String imageURL, int guidelines, String tags) {
        this.name = name;
        this.description = description;
        this.imageURL = imageURL;
        this.guidelines = guidelines;
        this.tags = tags;
    }

    protected Accident(Parcel in) {
        ID = in.readInt();
        name = in.readString();
        description = in.readString();
        imageURL = in.readString();
        guidelines = in.readInt();
    }

    public static final Creator<Accident> CREATOR = new Creator<Accident>() {
        @Override
        public Accident createFromParcel(Parcel in) {
            return new Accident(in);
        }

        @Override
        public Accident[] newArray(int size) {
            return new Accident[size];
        }
    };

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public int getGuidelines() {
        return guidelines;
    }

    public void setGuidelines(int guidelines) {
        this.guidelines = guidelines;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(ID);
        parcel.writeString(name);
        parcel.writeString(description);
        parcel.writeString(imageURL);
        parcel.writeInt(guidelines);
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }
}
