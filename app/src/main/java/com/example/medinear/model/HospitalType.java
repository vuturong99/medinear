package com.example.medinear.model;

import android.os.Parcel;
import android.os.Parcelable;

public class HospitalType implements Parcelable {
    private String type;
    private boolean checked;
    private String displayedText;

    public HospitalType(String type, String displayedText, boolean checked) {
        this.type = type;
        this.checked = checked;
        this.displayedText = displayedText;
    }

    private HospitalType(Parcel in) {
        type = in.readString();
        checked = in.readByte() != 0;
    }

    public static final Creator<HospitalType> CREATOR = new Creator<HospitalType>() {
        @Override
        public HospitalType createFromParcel(Parcel in) {
            return new HospitalType(in);
        }

        @Override
        public HospitalType[] newArray(int size) {
            return new HospitalType[size];
        }
    };


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(type);
        parcel.writeByte((byte) (checked ? 1 : 0));
    }

    public String getDisplayedText() {
        return displayedText;
    }

    public void setDisplayedText(String displayedText) {
        this.displayedText = displayedText;
    }
}
