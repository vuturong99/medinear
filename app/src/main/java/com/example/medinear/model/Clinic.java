package com.example.medinear.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

@Entity(tableName = "clinics_table")
public class Clinic implements ClusterItem, Parcelable {

    @PrimaryKey
    @ColumnInfo(name = "id")
    private int ID;

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "address")
    private String address;

    @ColumnInfo(name = "imageURL")
    private String imageURL;

    @ColumnInfo(name = "rating")
    private double rating;

    @ColumnInfo(name = "lat")
    private double latitude;

    @ColumnInfo(name = "long")
    private double longitude;

    @ColumnInfo(name = "phone")
    private String phone;

    @ColumnInfo(name = "type")
    private String type;

    @ColumnInfo(name = "eng_name")
    private String eng_name;

    @ColumnInfo(name = "monday_time")
    private
    String monday_time;

    @ColumnInfo(name = "tuesday_time")
    private
    String tuesday_time;

    @ColumnInfo(name = "wednesday_time")
    private
    String wednesday_time;

    @ColumnInfo(name = "thursday_time")
    private
    String thursday_time;

    @ColumnInfo(name = "friday_time")
    private
    String friday_time;

    @ColumnInfo(name = "saturday_time")
    private
    String saturday_time;

    @ColumnInfo(name = "sunday_time")
    private
    String sunday_time;

    @ColumnInfo(name = "open_status")
    private boolean isOpen;

    public Clinic() {

    }

    @Ignore
    public Clinic(int ID, String name, String address, String imageURL, double rating, double latitude, double longitude, String phone, String type, String eng_name, String monday_time, String tuesday_time, String wednesday_time, String thursday_time, String friday_time, String saturday_time, String sunday_time) {
        this.ID = ID;
        this.name = name;
        this.address = address;
        this.imageURL = imageURL;
        this.rating = rating;
        this.latitude = latitude;
        this.longitude = longitude;
        this.phone = phone;
        this.type = type;
        this.eng_name = eng_name;
        this.monday_time = monday_time;
        this.tuesday_time = tuesday_time;
        this.wednesday_time = wednesday_time;
        this.thursday_time = thursday_time;
        this.friday_time = friday_time;
        this.saturday_time = saturday_time;
        this.sunday_time = sunday_time;
    }


    protected Clinic(Parcel in) {
        ID = in.readInt();
        name = in.readString();
        address = in.readString();
        imageURL = in.readString();
        rating = in.readDouble();
        latitude = in.readDouble();
        longitude = in.readDouble();
        phone = in.readString();
        type = in.readString();
        eng_name = in.readString();
        monday_time = in.readString();
        tuesday_time = in.readString();
        wednesday_time = in.readString();
        thursday_time = in.readString();
        friday_time = in.readString();
        saturday_time = in.readString();
        sunday_time = in.readString();
    }

    public static final Creator<Clinic> CREATOR = new Creator<Clinic>() {
        @Override
        public Clinic createFromParcel(Parcel in) {
            return new Clinic(in);
        }

        @Override
        public Clinic[] newArray(int size) {
            return new Clinic[size];
        }
    };

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    @NonNull
    public String getAddress() {
        return address;
    }

    public void setAddress(@NonNull String address) {
        this.address = address;
    }

    @NonNull
    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(@NonNull String imageURL) {
        this.imageURL = imageURL;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double  longitude) {
        this.longitude = longitude;
    }

    @NonNull
    public String getPhone() {
        return phone;
    }

    public void setPhone(@NonNull String phone) {
        this.phone = phone;
    }

    @NonNull
    public String getType() {
        return type;
    }

    public void setType(@NonNull String type) {
        this.type = type;
    }

    public String getEng_name() {
        return eng_name;
    }

    public void setEng_name(String eng_name) {
        this.eng_name = eng_name;
    }

    public String getMonday_time() {
        return monday_time;
    }

    public void setMonday_time(String monday_time) {
        this.monday_time = monday_time;
    }

    public String getTuesday_time() {
        return tuesday_time;
    }

    public void setTuesday_time(String tuesday_time) {
        this.tuesday_time = tuesday_time;
    }

    public String getWednesday_time() {
        return wednesday_time;
    }

    public void setWednesday_time(String wednesday_time) {
        this.wednesday_time = wednesday_time;
    }

    public String getThursday_time() {
        return thursday_time;
    }

    public void setThursday_time(String thursday_time) {
        this.thursday_time = thursday_time;
    }

    public String getFriday_time() {
        return friday_time;
    }

    public void setFriday_time(String friday_time) {
        this.friday_time = friday_time;
    }

    public String getSaturday_time() {
        return saturday_time;
    }

    public void setSaturday_time(String saturday_time) {
        this.saturday_time = saturday_time;
    }

    public String getSunday_time() {
        return sunday_time;
    }

    public void setSunday_time(String sunday_time) {
        this.sunday_time = sunday_time;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(ID);
        parcel.writeString(name);
        parcel.writeString(address);
        parcel.writeString(imageURL);
        parcel.writeDouble(rating);
        parcel.writeDouble(latitude);
        parcel.writeDouble(longitude);
        parcel.writeString(phone);
        parcel.writeString(type);
        parcel.writeString(eng_name);
        parcel.writeString(monday_time);
        parcel.writeString(tuesday_time);
        parcel.writeString(wednesday_time);
        parcel.writeString(thursday_time);
        parcel.writeString(friday_time);
        parcel.writeString(saturday_time);
        parcel.writeString(sunday_time);
    }

    @NonNull
    @Override
    public LatLng getPosition() {
        return new LatLng(getLatitude(), getLongitude());
    }

    @Nullable
    @Override
    public String getTitle() {
        return this.name;
    }

    @Nullable
    @Override
    public String getSnippet() {
        return String.valueOf(ID);
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }
}
