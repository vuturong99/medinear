package com.example.medinear.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "opening_hours_table")
public class OpeningHours {
    @PrimaryKey
    @ColumnInfo(name = "clinic_id")
    String clinic_id;

    @ColumnInfo(name = "monday_time")
    String monday_time;

    @ColumnInfo(name = "tuesday_time")
    String tuesday_time;

    @ColumnInfo(name = "wednesday_time")
    String wednesday_time;

    @ColumnInfo(name = "thursday_time")
    String thursday_time;

    @ColumnInfo(name = "friday_time")
    String friday_time;

    @ColumnInfo(name = "saturday_time")
    String saturday_time;

    @ColumnInfo(name = "sunday_time")
    String sunday_time;

}
