package com.example.medinear.api.air_quality_model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AirStatus {
    @SerializedName("city_name")
    private String city_name;

    @SerializedName("data")
    private List<Data> data;

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }
}
