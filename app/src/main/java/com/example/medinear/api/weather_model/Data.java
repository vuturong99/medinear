package com.example.medinear.api.weather_model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Data {
    @SerializedName("temp")
    String temp;

    @SerializedName("datetime")
    String datetime;

    @SerializedName("rh")
    String relative_humidity;

    @SerializedName("wind_spd")
    String wind_speed;

    @SerializedName("weather")
    WeatherDescription weatherDescriptionList;

    public WeatherDescription getWeatherDescriptionList() {
        return weatherDescriptionList;
    }

    public void setWeatherDescriptionList(WeatherDescription weatherDescriptionList) {
        this.weatherDescriptionList = weatherDescriptionList;
    }

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getRelative_humidity() {
        return relative_humidity;
    }

    public void setRelative_humidity(String relative_humidity) {
        this.relative_humidity = relative_humidity;
    }

    public String getWind_speed() {
        return wind_speed;
    }

    public void setWind_speed(String wind_speed) {
        this.wind_speed = wind_speed;
    }
}
