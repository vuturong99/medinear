package com.example.medinear.api.weather_model;

import com.google.gson.annotations.SerializedName;

public class WeatherDescription {
    @SerializedName("description")
    String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
