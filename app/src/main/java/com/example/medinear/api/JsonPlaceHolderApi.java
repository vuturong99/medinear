package com.example.medinear.api;

import com.example.medinear.api.air_quality_model.AirStatus;
import com.example.medinear.api.weather_model.Weather;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface JsonPlaceHolderApi {
    @GET("airquality")
    Call<AirStatus> getStatus(@Query("lat") double lat, @Query("lon") double lon, @Query("key") String key);

    @GET("daily")
    Call<Weather> getWeather(@Query("lat") double lat, @Query("lon") double lon, @Query("key") String key);
}
