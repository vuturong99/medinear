package com.example.medinear.cluster;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;

import androidx.annotation.NonNull;

import com.example.medinear.R;
import com.example.medinear.model.Clinic;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;

public class CustomClusterRenderer extends DefaultClusterRenderer<Clinic> {

    private Context context;
    public CustomClusterRenderer(Context context, GoogleMap map, ClusterManager<Clinic> clusterManager) {
        super(context, map, clusterManager);
        this.context = context;
    }


    @Override
    protected void onBeforeClusterItemRendered(@NonNull Clinic item, @NonNull MarkerOptions markerOptions) {
        super.onBeforeClusterItemRendered(item, markerOptions);
        int height = 120;
        int width = 120;
        BitmapDrawable bitmapDrawable = (BitmapDrawable) context.getResources().getDrawable(R.drawable.ic_marker_red);
        Bitmap b = bitmapDrawable.getBitmap();
        Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);
        final BitmapDescriptor markerDescriptor = BitmapDescriptorFactory.fromBitmap(smallMarker);
        markerOptions.icon(markerDescriptor);
    }

    @Override
    protected void onBeforeClusterRendered(@NonNull Cluster<Clinic> cluster, @NonNull MarkerOptions markerOptions) {
        super.onBeforeClusterRendered(cluster, markerOptions);
        int height = 120;
        int width = 120;
        BitmapDrawable bitmapDrawable = (BitmapDrawable) context.getResources().getDrawable(R.drawable.ic_marker_blue);
        Bitmap b = bitmapDrawable.getBitmap();
        Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);
        final BitmapDescriptor markerDescriptor = BitmapDescriptorFactory.fromBitmap(smallMarker);
        markerOptions.icon(markerDescriptor);

    }

    @Override
    protected void onClusterRendered(@NonNull Cluster<Clinic> cluster, @NonNull Marker marker) {
        super.onClusterRendered(cluster, marker);
        marker.setTag(cluster);
        marker.setTitle(cluster.getSize() + " Hospitals");
        marker.setSnippet("Hello guys");
    }
}
