package com.example.medinear.view.fragment;

import android.Manifest;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.example.medinear.cluster.CustomClusterRenderer;
import com.example.medinear.database.ClinicRoomDatabase;
import com.example.medinear.directionapi.FetchURL;
import com.example.medinear.R;

import com.example.medinear.directionapi.TaskLoadedCallback;
import com.example.medinear.model.Clinic;
import com.example.medinear.view.activity.ClusteredClinicsActivity;
import com.example.medinear.view.activity.DetailActivity;
import com.example.medinear.view.activity.HomeActivity;
import com.example.medinear.view.adapter.ClinicAdapter;
import com.example.medinear.viewmodel.ClinicViewModel;
import com.example.medinear.viewmodel.LocationViewModel;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;

import com.google.android.gms.maps.OnMapReadyCallback;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;

import static android.content.Context.LOCATION_SERVICE;

public class MapFragment extends Fragment implements OnMapReadyCallback, LocationListener, TaskLoadedCallback {

    private ClusterManager<Clinic> mClusterManager;
    private GoogleMap mGoogleMap;
    private LocationListener locationListener;
    private LocationManager locationManager;
    private static final int REQUEST_CODE = 101;
    private boolean cameraSet = false;
    private Marker marker;
    private LinearLayout progressBar;
    private Polyline currentPolyline;
    private ClinicViewModel mClinicViewModel;
    private LatLng mOrigin;
    private LocationViewModel viewModel;
    private LatLng mDest;
    private LatLng cordinates;
    private ClinicAdapter mClinicAdapter;
    private HomeFragment homeFragment;
    private ImageView moveToCurrentPositionBtn;
    CardView clinicCardDetail;
    LinearLayout linearCard;
    TextView clinicName;
    TextView ratingNumber;
    MaterialRatingBar ratingBar;
    TextView directionsButton;
    TextView callButton;
    Clinic mClinic;
    TextView closeButton;
    LatLng currentCoordinates = new LatLng(21,105);
    boolean drawHere = false;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_map, container, false);

        //Initialize mapView

        mClinicViewModel = new ViewModelProvider(requireActivity()).get(ClinicViewModel.class);

        progressBar = requireActivity().findViewById(R.id.loadingDialog);
        MapView mMapView = mView.findViewById(R.id.mapView);
        if (mMapView != null) {
            mMapView.onCreate(null);
            mMapView.onResume();
            mMapView.getMapAsync(this);
        }

        fetchLocation();

        initViews(mView);

        return mView;
    }

    private void initViews(View view) {
        moveToCurrentPositionBtn = view.findViewById(R.id.moveToCurrentPosition);
        moveToCurrentPositionBtn.setOnClickListener(moveToCurrentPosition);
        clinicCardDetail = view.findViewById(R.id.clinic_detail);
        clinicCardDetail.animate()
                .alpha(0.0f)
                .setDuration(400);
        ratingBar = view.findViewById(R.id.clinicRating);
        clinicName = view.findViewById(R.id.clinic_name);
        ratingNumber = view.findViewById(R.id.rateNumber);
        callButton = view.findViewById(R.id.callButton);
        directionsButton = view.findViewById(R.id.navigateButton);
        linearCard = view.findViewById(R.id.linearCard);
        closeButton = view.findViewById(R.id.closeButton);

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clinicCardDetail.animate()
                        .alpha(0.0f)
                        .setDuration(400).setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        clinicCardDetail.setVisibility(View.GONE);
                        clinicCardDetail.animate().setListener(null);
                    }
                });

            }
        });
        linearCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Clinic clinic = mClinicViewModel.getClinicByID(mClinic.getID());
                Intent intent = new Intent(getContext(), DetailActivity.class);
                intent.putExtra("clinic", clinic);
                requireActivity().startActivityForResult(intent, 1);
            }
        });
    }


    public void drawWayHere(LatLng originLatLng, LatLng destinationLatLng) {
        MarkerOptions destination = new MarkerOptions().position(destinationLatLng);
        MarkerOptions origin = new MarkerOptions().position(originLatLng);

        mOrigin = originLatLng;
        mDest = destinationLatLng;

        String url = getUrl(origin.getPosition(), destination.getPosition());
        new FetchURL(this).execute(url, "driving");
    }
    public void drawWay(MarkerOptions origin, LatLng originLatLng) {
        drawHere = false;
        Clinic clinic = ((HomeActivity) requireActivity()).getClinic();
        assert clinic != null;
        int clinicID = clinic.getID();
        LatLng clinicLatLng = new LatLng(mClinicViewModel.getClinicByID(clinicID).getLatitude(),
                mClinicViewModel.getClinicByID(clinicID).getLongitude());

        mOrigin = originLatLng;
        mDest = clinicLatLng;

        //Draw route function
        MarkerOptions destination = new MarkerOptions().position(clinicLatLng);
        String url = getUrl(origin.getPosition(), destination.getPosition());
        new FetchURL(this).execute(url, "driving");

    }

    //Immediately trigger this function when navigate is called
    public void drawWayInActivity(LatLng originLatLng, Clinic clinic) {
        drawHere = false;
        LatLng destinationLatLng = new LatLng(mClinicViewModel.getClinicByID(clinic.getID()).getLatitude(),
                mClinicViewModel.getClinicByID(clinic.getID()).getLongitude());
        MarkerOptions originOption = new MarkerOptions().position(new LatLng(originLatLng.latitude, originLatLng.longitude));

        mOrigin = originLatLng;
        mDest = destinationLatLng;

        MarkerOptions destinationOption = new MarkerOptions().position(destinationLatLng);
        String url = getUrl(originOption.getPosition(), destinationOption.getPosition());
        new FetchURL(this).execute(url, "driving");
    }

    private String getUrl(LatLng origin, LatLng destination) {
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;
        String str_dest = "destination=" + destination.latitude + "," + destination.longitude;
        String mode = "mode=" + "driving";
        String parameters = str_origin + "&" + str_dest + "&" + mode;
        String output = "json";
        if (requireActivity().getResources() != null) {
            return "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters + "&key=" + requireActivity().getResources().getString(R.string.google_maps_key);
        } else {
            return "";
        }

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewModel = new ViewModelProvider(requireActivity()).get(LocationViewModel.class);
    }


    @Override
    public void onTaskDone(Object... values) {

        if (Arrays.toString(values).equals("[null]")) {
            requireActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(getActivity(), "No internet connection", Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            if (currentPolyline != null) {
                currentPolyline.remove();
            }
            currentPolyline = mGoogleMap.addPolyline((PolylineOptions) values[0]);
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            builder.include(mOrigin);
            builder.include(mDest);
            LatLngBounds bounds = builder.build();
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 50);
            mGoogleMap.animateCamera(cu);
            progressBar.setVisibility(View.GONE);
        }
    }

    private void fetchLocation() {
        locationManager = (LocationManager) requireActivity().getSystemService(LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(requireActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(requireActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 101);
            return;
        }

        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {


                Toast.makeText(getActivity(), "dasdasd", Toast.LENGTH_SHORT).show();
                double latitude = location.getLatitude();
                double longitude = location.getLongitude();
                cordinates = new LatLng(latitude,longitude);

                LatLng latLng = new LatLng(latitude, longitude);

                currentCoordinates = latLng;

                mClinicViewModel.setClinics(returnClinics(latLng));


                viewModel.setCurrentLocation(latLng);

                MarkerOptions origin = new MarkerOptions().position(new LatLng(latitude, longitude));


                if (((HomeActivity) requireActivity()).getClinic() != null && !drawHere) {
                    drawWay(origin, latLng);
//                    drawWayInActivity(latLng, mClinic);
                }

                if (drawHere) {
                    drawWayInActivity(latLng, mClinic);
                }


                if (marker != null) {
                    marker.remove();
                    marker = mGoogleMap.addMarker(new MarkerOptions().
                            position(latLng)
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
                } else {
                    marker = mGoogleMap.addMarker(new MarkerOptions().
                            position(latLng)
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));

                    //Prevent camera from jumping to the current position
                    if (!cameraSet) {
                        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 14.0f));
                        cameraSet = true;
                    }
                }
            }


            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }
        };


        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);

    }

    private List<Clinic> returnClinics(LatLng mOrigin) {
        final Map<Clinic, Float> hashMap = new HashMap<>();

        ClinicRoomDatabase db = ClinicRoomDatabase.getDatabase(getContext());
        List<Clinic> clinics = db.clinicDao().getClinics();

        for (Clinic clinic : clinics) {
            float[] results = new float[10];
            Location.distanceBetween(mOrigin.latitude, mOrigin.longitude, clinic.getLatitude(), clinic.getLongitude(), results);
            hashMap.put(clinic, results[0]);
            Log.i("Distance", clinic.getName() + results[0]);
        }

        Log.i("HashMap", hashMap.keySet().toString());

        List<Float> distances = new ArrayList<>(hashMap.values());
        Collections.sort(distances);

        List<Clinic> clinicsSorted = new ArrayList<>();

        for (int i = 0; i < distances.size(); i++) {
            Log.i("Distance", distances.get(i) + " ");
            for (Clinic clinic : hashMap.keySet()) {
                if (distances.get(i).equals(hashMap.get(clinic))) {
                    clinicsSorted.add(clinic);
                    break;
                }
            }
        }

        return clinicsSorted;

    }

    private LatLng returnCordinates(LatLng cordinates) {
        return cordinates;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        setUpClusterer();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                fetchLocation();
            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }


    private void setUpClusterer() {
        mClusterManager = new ClusterManager<>(requireActivity(), mGoogleMap);

        mClusterManager.setOnClusterItemClickListener(new ClusterManager.OnClusterItemClickListener<Clinic>() {
            @Override
            public boolean onClusterItemClick(Clinic item) {
                final LatLng clinicLatLng = new LatLng(item.getLatitude(), item.getLongitude());
                mClinic = item;
                clinicName.setText(item.getName());
                ratingBar.setRating((float)(mClinic.getRating()));
                ratingNumber.setText(String.format(Locale.getDefault(), "%.2f", item.getRating()));

                CameraUpdate location = CameraUpdateFactory.newLatLngZoom(clinicLatLng, 14.0f);
                mGoogleMap.animateCamera(location);

                clinicCardDetail.animate()
                        .alpha(1.0f)
                        .setDuration(400);
                clinicCardDetail.setVisibility(View.VISIBLE);



                callButton.setOnClickListener(mCallButtonClick);
                directionsButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        drawHere = true;
                        clinicCardDetail.animate()
                                .alpha(0.0f)
                                .setDuration(400);
                        drawWayHere(currentCoordinates,clinicLatLng);
                    }
                });
                return true;
            }
        });
        mClusterManager.setOnClusterItemInfoWindowClickListener(new ClusterManager.OnClusterItemInfoWindowClickListener<Clinic>() {
            @Override
            public void onClusterItemInfoWindowClick(Clinic item) {
                assert item.getSnippet() != null;
                Clinic clinic = mClinicViewModel.getClinicByID(Integer.valueOf(item.getSnippet()));
                Intent intent = new Intent(getContext(), DetailActivity.class);
                intent.putExtra("clinic", clinic);
                requireActivity().startActivityForResult(intent, 1);
            }
        });


        mClusterManager.setOnClusterInfoWindowClickListener(new ClusterManager.OnClusterInfoWindowClickListener<Clinic>() {
            @Override
            public void onClusterInfoWindowClick(Cluster<Clinic> cluster) {
                Intent intent = new Intent(getContext(), ClusteredClinicsActivity.class);
                ArrayList<Clinic> clinics = new ArrayList<>(cluster.getItems());
                intent.putParcelableArrayListExtra("clinics",clinics);
                Log.i("Clinics",clinics.size() + "");
                requireActivity().startActivityForResult(intent, 1);
            }
        });

        final CustomClusterRenderer renderer = new CustomClusterRenderer(requireActivity(), mGoogleMap, mClusterManager);

        mGoogleMap.setOnCameraIdleListener(mClusterManager);
        mGoogleMap.setOnMarkerClickListener(mClusterManager);

        mClusterManager.setRenderer(renderer);


        addItems();
    }

    private void addItems() {
        //Add nearby markers
        mClinicViewModel.getAllClinics().observe(getViewLifecycleOwner(), new Observer<List<Clinic>>() {
            @Override
            public void onChanged(List<Clinic> clinics) {
                mClusterManager.addItems(clinics);
                mClusterManager.cluster();

            }
        });
    }



    private View.OnClickListener moveToCurrentPosition = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (cordinates!=null) {
                CameraUpdate location = CameraUpdateFactory.newLatLngZoom(cordinates, 14.0f);
                mGoogleMap.animateCamera(location);
            }
        }
    };

    private final View.OnClickListener mCallButtonClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + mClinic.getPhone()));
            startActivity(intent);
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
        Toast.makeText(getActivity(), "DEAD", Toast.LENGTH_SHORT).show();
        locationManager.removeUpdates(locationListener);
    }

}


