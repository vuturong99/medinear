package com.example.medinear.view.activity;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.medinear.R;
import com.example.medinear.model.Clinic;
import com.example.medinear.model.HospitalType;
import com.example.medinear.view.adapter.ClinicAdapter;
import com.example.medinear.viewmodel.ClinicViewModel;
import com.google.android.gms.maps.model.LatLng;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class SearchActivity extends AppCompatActivity {

    private ClinicViewModel mClinicViewModel;
    private List<Clinic> mClinicList = new ArrayList<>();
    private ClinicAdapter mClinicAdapter;
    private EditText searchET;
    private CardView searchBar;
    private RecyclerView mRecyclerView;
    private View view;
    private LatLng currentLocation;
    ImageView filterButton;
    TextView title;
    ImageView backButton;
    private ArrayList<String> hospitalTypes;
    private boolean openClinics = false;
    private int sortBy = 0;

    final static int RETURN_CODE_FILTER = 2;
    final static int RETURN_CODE_DETAIL = 1;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        setContentView(R.layout.activity_search);
        loadActionBar();
        initView();

    }

    private void initView() {
        currentLocation = getIntent().getParcelableExtra("Coordinates");

        mRecyclerView = findViewById(R.id.list_result);
        mClinicAdapter = new ClinicAdapter(mClinicList, mItemClickListener,getApplicationContext());
        mClinicViewModel = new ViewModelProvider(SearchActivity.this).get(ClinicViewModel.class);
        mRecyclerView.setAdapter(mClinicAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        searchBar = findViewById(R.id.search_bar);
        searchET = findViewById(R.id.search_editText);

        if (currentLocation == null) {
            searchET.addTextChangedListener(offlineTextWatcher);
        } else {
            mClinicAdapter.setCurrentLocation(currentLocation);
            searchET.addTextChangedListener(onlineTextWatcher);
        }

        filterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(SearchActivity.this, FilterActivity.class),RETURN_CODE_FILTER);
            }
        });
    }

    private final ClinicAdapter.ItemClickListener mItemClickListener = new ClinicAdapter.ItemClickListener() {
        @Override
        public void onClick(Clinic clinic) {
            Log.i("HomeFragment", "Clinic details " + clinic.getName() + " " + clinic.getAddress());
            Intent intent = new Intent(getApplicationContext(), DetailActivity.class);
            intent.putExtra("clinic", clinic);
            startActivityForResult(intent, RETURN_CODE_DETAIL);
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RETURN_CODE_DETAIL) {
            if (data != null) {
                Clinic clinic = data.getParcelableExtra("clinic_return");
                Intent clinicIntent = new Intent();
                clinicIntent.putExtra("clinic_return", clinic);
                setResult(Activity.RESULT_OK, clinicIntent);
                finish();
            }
        }

        if (requestCode == RETURN_CODE_FILTER) {
            if (data != null) {
                openClinics = data.getBooleanExtra("filter_openClinics",false);
                Log.i("OPEN CLINICS", openClinics + "");

                if (data.getBooleanExtra("filter_sortAlphaDesc",false)) {
                    sortBy = 2;
                } else if (data.getBooleanExtra("filter_sortAlphaAsc",false)) {
                    sortBy = 3;
                } else if (data.getBooleanExtra("filter_sortRating",false)){
                    sortBy = 1;
                } else {
                    sortBy = 0;
                }

                hospitalTypes = data.getStringArrayListExtra("filter_type");


                filt(sortBy, openClinics, hospitalTypes);


            }

        }

    }

    private void filt(int sortBy, boolean openClinics, ArrayList<String> hospitalTypes) {
        List<Clinic> filtedClinics;
        List<Clinic> filtedClinics2 = new ArrayList<>();

        if (sortBy == 1) {
            mClinicList = mClinicViewModel.getHighRatingsClinics();
        }

        if (sortBy == 2) {
            mClinicList = mClinicViewModel.getReAlphabetizedClinics();
        }

        if (sortBy == 3) {
            mClinicList = mClinicViewModel.getAlphabetizedClinics();
        }

        if (sortBy == 0) {
            mClinicList = mClinicViewModel.getAlphabetizedClinics();
        }

        filtedClinics = mClinicList;

        timeMaster(mClinicList);

        if (openClinics) {
            if (hospitalTypes.size() > 0) {
                for (Clinic clinic: mClinicList) {
                    if (clinic.isOpen() && hospitalTypes.contains(clinic.getType())) {
                        Log.i("OPEN CLINICS", clinic.getName());
                        filtedClinics2.add(clinic);
                        mClinicAdapter.setClinics(filtedClinics2);
                    }
                }
            } else {
                for (Clinic clinic : mClinicList) {
                    if (clinic.isOpen()) {
                        Log.i("OPEN CLINICS", clinic.getName());
                        filtedClinics2.add(clinic);
                        mClinicAdapter.setClinics(filtedClinics2);
                    }
                }
            }
        } else {
            if (hospitalTypes.size() != 0) {
                for (Clinic clinic : mClinicList) {
                    if (hospitalTypes.contains(clinic.getType())) {
                        filtedClinics2.add(clinic);
                    }
                }
                mClinicAdapter.setClinics(filtedClinics2);
            } else {
                mClinicAdapter.setClinics(filtedClinics);
            }
        }









    }

    private void loadActionBar() {
        if (this.getSupportActionBar() != null) {
            this.getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setCustomView(R.layout.custom_action_bar_action);
            getSupportActionBar().setElevation(5);
            view = getSupportActionBar().getCustomView();
            title = view.findViewById(R.id.title);
            title.setText(R.string.search);
            backButton = view.findViewById(R.id.back_button);
            backButton.setOnClickListener(backListener);
            filterButton = view.findViewById(R.id.action_button);
            filterButton.setImageResource(R.drawable.ic_filter);
        }
    }

    private List<Clinic> returnClinics(LatLng mOrigin, List<Clinic> clinics) {
        final Map<Clinic, Float> hashMap = new HashMap<>();

        for (Clinic clinic : clinics) {
            float[] results = new float[10];
            Location.distanceBetween(mOrigin.latitude, mOrigin.longitude, clinic.getLatitude(), clinic.getLongitude(), results);
            hashMap.put(clinic, results[0]);
            Log.i("Distance", clinic.getName() + results[0]);
        }

        Log.i("HashMap", hashMap.keySet().toString());

        List<Float> distances = new ArrayList<>(hashMap.values());
        Collections.sort(distances);

        List<Clinic> clinicsSorted = new ArrayList<>();

        for (int i = 0; i < distances.size(); i++) {
            Log.i("Distance", distances.get(i) + " ");
            for (Clinic clinic : hashMap.keySet()) {
                if (distances.get(i).equals(hashMap.get(clinic))) {
                    clinicsSorted.add(clinic);
                    break;
                }
            }
        }

        return clinicsSorted;

    }

    View.OnClickListener backListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            finish();
        }
    };

    //Query offline, when location = null
    TextWatcher offlineTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            if (editable.toString().length() != 0) {
                mClinicViewModel.searchClinics(editable.toString()).observe(SearchActivity.this, new Observer<List<Clinic>>() {
                    @Override
                    public void onChanged(List<Clinic> clinics) {
                        mClinicAdapter.setClinics(clinics);
                        mClinicAdapter.notifyDataSetChanged();
                    }
                });
            }
        }
    };

    //Query with live database, when location != null
    TextWatcher onlineTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            if (editable.toString().length() != 0) {
                mClinicViewModel.searchClinics(editable.toString()).observe(SearchActivity.this, new Observer<List<Clinic>>() {
                    @Override
                    public void onChanged(List<Clinic> clinics) {
                        mClinicAdapter.setClinics(returnClinics(currentLocation, clinics));
                        mClinicAdapter.notifyDataSetChanged();
                    }
                });
            }

        }
    };

    public void timeMaster(List<Clinic> clinics) {
        String getTime;
        Date now = new Date();
        Date time1;
        Date time2;
        String timeNow;


        SimpleDateFormat simpleDateformat = new SimpleDateFormat("E", Locale.US);
        SimpleDateFormat simpleTimeFormat = new SimpleDateFormat("hh:mm a",Locale.US);

        for (Clinic clinic : clinics) {
            switch (simpleDateformat.format(now)) {
                case "Mon":
                    getTime = clinic.getMonday_time();
                    break;
                case "Tue":
                    getTime = clinic.getTuesday_time();
                    break;
                case "Wed":
                    getTime = clinic.getWednesday_time();
                    break;
                case "Thu":
                    getTime = clinic.getThursday_time();
                    break;
                case "Fri":
                    getTime = clinic.getFriday_time();
                    break;
                case "Sat":
                    getTime = clinic.getSaturday_time();
                    break;
                case "Sun":
                    getTime = clinic.getSunday_time();
                    break;
                default:
                    getTime = "Open: 7:30 AM - Close: 4:30 PM";
            }

            if (getTime.contains("24")) {
                clinic.setOpen(true);
            } else if (getTime.contains("Closed")) {
                clinic.setOpen(false);
            } else {
                String[] timeStrings = getTime.split(" ");

                try {
                    time1 = simpleTimeFormat.parse(timeStrings[1] + " " + timeStrings[2]);
                    time2 = simpleTimeFormat.parse(timeStrings[timeStrings.length - 2] + " " + timeStrings[timeStrings.length - 1]);
                    timeNow = simpleTimeFormat.format(new Date());
                    now = simpleTimeFormat.parse(timeNow);
                    Log.i("TIME",now + "");
                    Log.i("TIME",timeNow);

                    if (time1.before(now) && time2.after(now)) {
                        clinic.setOpen(true);
                    } else {
                        clinic.setOpen(false);
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}

