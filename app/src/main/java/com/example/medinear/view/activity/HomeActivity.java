package com.example.medinear.view.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.medinear.ScrollInterface;
import com.example.medinear.database.ClinicRoomDatabase;
import com.example.medinear.model.Clinic;
import com.example.medinear.model.User;
import com.example.medinear.view.fragment.AirQualityFragment;
import com.example.medinear.view.fragment.FirstAidFragment;
import com.example.medinear.view.fragment.HomeFragment;
import com.example.medinear.view.fragment.MapFragment;
import com.example.medinear.view.fragment.ProfileFragment;
import com.example.medinear.R;
import com.example.medinear.viewmodel.ClinicViewModel;
import com.example.medinear.viewmodel.LocationViewModel;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.functions.FirebaseFunctions;
import com.google.firebase.functions.FirebaseFunctionsException;
import com.google.firebase.functions.HttpsCallableResult;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.gson.Gson;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class HomeActivity extends AppCompatActivity {
    View view;
    Clinic clinicReturn;
    boolean zoomOnce = false;
    TextView title;
    FirebaseFunctions mFunctions;
    LinearLayout loadingDialog;
    FragmentTransaction fT;
    FragmentTransaction ft2;
    Fragment homeFragment = new HomeFragment();
    Fragment mapFragment = new MapFragment();
    Fragment firstAidFragment = new FirstAidFragment();
    Fragment profileFragment = new ProfileFragment();
    Fragment aqiFragment = new AirQualityFragment();
    FirebaseAuth mAuth;
    ScrollInterface scrollListener;
    ClinicViewModel mClinicViewModel;
    ImageView searchButton;
    LatLng currentLatLng;
    ImageView backButton;
    ValueEventListener mPostListener;
    DatabaseReference mDatabaseReference;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference mClinicReference;
    ClinicRoomDatabase mClinicRoomDatabase;
    private StorageReference storageReference;
    FirebaseStorage firebaseStorage;
    FirebaseAuth firebaseAuth;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        checkForChanges();
        firebaseAuth();
        initDatabase();
        initViewModel();
        initView();
        firebaseAuth = FirebaseAuth.getInstance();
        updateUserWithName(Objects.requireNonNull(firebaseAuth.getCurrentUser()).getEmail());
        fetchHospital();

//        rate();





//        Places.initialize(getApplicationContext(),getString(R.string.google_maps_key));
//        AutocompleteSupportFragment autocompleteFragment = (AutocompleteSupportFragment) getSupportFragmentManager().findFragmentById(R.id.autocomplete_fragment);
//
//        assert autocompleteFragment != null;
//        autocompleteFragment.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.NAME));
//
//        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
//            @Override
//            public void onPlaceSelected(@NonNull Place place) {
//                Log.i("LOL",place.getName());
//            }
//
//            @Override
//            public void onError(@NonNull Status status) {
//                Log.i("LOL","CANT" + status);
//            }
//        });


//        message();


        //Set custom action bar

        loadActionBar();
        getDistance();


        title = view.findViewById(R.id.title);


//        SearchView searchView = findViewById(R.id.searchView);
//        ImageView searchIconHint = searchView.findViewById(android.appwidget.);
//        searchIconHint.setImageDrawable(null)       ;

        addFragments();
        setBottomNavigationView();
        //Set default bottom navigation

        //Set default fragment
//        Bundle bundle = new Bundle();
//        String lol = getIntent().getStringExtra("name");
//        bundle.putString("Display name", getIntent().getStringExtra("name"));
//        profileFragment.setArguments(bundle);


    }

    private void fetchHospital() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        if (!preferences.getBoolean("firstTime", false)) {
            fetch();
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean("firstTime", true);
            editor.apply();
        }



    }




    private void initDatabase() {
        firebaseDatabase = FirebaseDatabase.getInstance();
        mDatabaseReference = firebaseDatabase.getReference();
        mClinicRoomDatabase = ClinicRoomDatabase.getDatabase(getApplicationContext());
        firebaseStorage = FirebaseStorage.getInstance();
        storageReference = firebaseStorage.getReference();
    }

    private void setBottomNavigationView() {
        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(navigationItemSelectedListener);
        bottomNavigationView.setOnNavigationItemReselectedListener(navigationItemReselectedListener);
    }

    private void addFragments() {
        fT = getSupportFragmentManager().beginTransaction();
        ft2 = getSupportFragmentManager().beginTransaction();
        fT.add(R.id.fragment_container, homeFragment, "homeFragment");
        fT.add(R.id.fragment_container, mapFragment, "mapFragment");
        fT.add(R.id.fragment_container, firstAidFragment);
        fT.add(R.id.fragment_container, profileFragment);
        fT.add(R.id.fragment_container,aqiFragment);
        fT.hide(mapFragment);
        fT.hide(firstAidFragment);
        fT.hide(profileFragment);
        fT.hide(aqiFragment);
        fT.commit();

    }

    private void initView() {

    }


    private void initViewModel() {

        mClinicViewModel = new ViewModelProvider(this).get(ClinicViewModel.class);


        LocationViewModel viewModel = new ViewModelProvider(this).get(LocationViewModel.class);
        viewModel.getCurrentLocation().observe(this, new Observer<LatLng>() {
            @Override
            public void onChanged(LatLng latLng) {
                currentLatLng = latLng;
            }
        });
    }

    private void firebaseAuth() {
        mAuth = FirebaseAuth.getInstance();

        if (mAuth.getCurrentUser() != null) {
            mAuth.getCurrentUser().getIdToken(true).addOnCompleteListener(new OnCompleteListener<GetTokenResult>() {
                @Override
                public void onComplete(@NonNull Task<GetTokenResult> task) {
                    if (task.isSuccessful()) {
                        assert task.getResult() != null;
                        String idToken = task.getResult().getToken();
                        Log.i("VuTST", idToken + "");
                    } else {
                        Toast.makeText(HomeActivity.this, "Something's gone wrong", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }

        mFunctions = FirebaseFunctions.getInstance("asia-east2");

    }

    private void getDistance() {
        float[] results = new float[10];
        Location.distanceBetween(21.0252624,105.7996404,21.0379521,105.8248075, results);
        Log.i("Location", results[0] + "HHEHEHEHHEE");
    }


    public void checkForChanges() {
        mClinicReference = FirebaseDatabase.getInstance().getReference().child("hospitals");

        mClinicReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
//                Log.i("Datazzz",snapshot.getKey() + snapshot.getValue());
                //                double lat = Double.parseDouble(snapshot.child("lat").getValue().toString());
//                double lng = Double.parseDouble(snapshot.child("lng").getValue().toString());
//                double rating = Double.parseDouble(snapshot.child("rating").getValue().toString());
//                int id = Integer.parseInt(snapshot.child("id").getValue().toString());
//                String imageURL = snapshot.child("imageURL").getValue().toString();
//                String name = snapshot.child("name").getValue().toString();
//                String address = snapshot.child("address").getValue().toString();

//                Clinic clinic = new Clinic(id,name,address,imageURL,rating,1200,200,lat,lng,"312123","DA");
//                Log.i("Datazzz",rating+ "");
//                mClinicRoomDatabase.clinicDao().insert(clinic);
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                double lat = Double.parseDouble(snapshot.child("lat").getValue().toString());
                double lng = Double.parseDouble(snapshot.child("lng").getValue().toString());
                double rating = Double.parseDouble(snapshot.child("rating").getValue().toString());
                int id = Integer.parseInt(snapshot.child("id").getValue().toString());
                String imageURL = snapshot.child("imageURL").getValue().toString();
                String name = snapshot.child("name").getValue().toString();
                String address = snapshot.child("address").getValue().toString();
                String phone = snapshot.child("phone").getValue().toString();
                String type = snapshot.child("type").getValue().toString();
                String eng_name = snapshot.child("eng_name").getValue().toString();
//                String mon = snapshot.child("monday").getValue().toString();
//                String tue = snapshot.child("tuesday").getValue().toString();
//                String wed = snapshot.child("wednesday").getValue().toString();
//                String thu = snapshot.child("thursday").getValue().toString();
//                String fri = snapshot.child("friday").getValue().toString();
//                String sat = snapshot.child("saturday").getValue().toString();
//                String sun = snapshot.child("sunday").getValue().toString();

                mClinicRoomDatabase.clinicDao().updateHospital(name,lat,lng,address,imageURL,phone,type,rating,eng_name,id);
                Log.i("Datazzz",rating+ "");
                Log.i("Datazzz",snapshot.toString());
//                mClinicRoomDatabase.clinicDao().insert(clinic);
//                    int id = (int) child.child("id").getValue();


//                    String name = (String) child.child("name").getValue();
//                    String address = (String) child.child("address").getValue();
//                    String imageURL = (String) child.child("imageURL").getValue();
//                    double rating = Double.parseDouble(child.child("rating").getValue().toString());
//                    Double lat = (Double) child.child("lat").getValue();
//                    Double lng = (Double) child.child("lng").getValue();
//                    String phone = (String) child.child("phone").getValue();
//
//                    assert name != null;
//                    assert address != null;
//                    assert imageURL != null;
//                    assert rating != null;
//                    assert lat != null;
//                    assert lng != null;
//                    assert phone != null;
//                    Clinic clinic = new Clinic(name,address,imageURL,rating,1200,1200,lat,lng,phone,"IDK");
//                    mClinicRoomDatabase.clinicDao().update(clinic);
////                    Log.i("Datazzz",mClinicRoomDatabase.clinicDao().getClinicByID(id)+ " ");                }
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot snapshot) {
                Log.i("Datazzz", "removed?");
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                Log.i("Datazzz", "moved?");
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.i("Datazzz", "cancelled?");
            }
        });
//        ValueEventListener listener = new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot snapshot) {
//                for (DataSnapshot child : snapshot.getChildren()) {
//                    Log.i("Datazzz",child.getKey() + " " + child.getValue());
//                }
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError error) {
//
//            }
//        };
//
//        mClinicReference.addValueEventListener(listener);

    }

    private void loadActionBar() {
        if (this.getSupportActionBar() != null) {
            this.getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setCustomView(R.layout.custom_action_bar_action);
            getSupportActionBar().setElevation(5);
            view = getSupportActionBar().getCustomView();
//            ImageView searchButton = view.findViewById(R.id.searchButton_AB);
//
//            searchButton.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    mActionBarListener.onClickListener();
//                }
//            });

            backButton = view.findViewById(R.id.back_button);
            backButton.setVisibility(View.GONE);
            searchButton = view.findViewById(R.id.action_button);
            searchButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(HomeActivity.this, SearchActivity.class);
                    intent.putExtra("Coordinates",currentLatLng);
                    startActivityForResult(intent,1);
                }
            });

        } else {
            Toast.makeText(this, "Unable to load action bar", Toast.LENGTH_SHORT).show();
        }
    }



    private BottomNavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    FragmentTransaction fT2 = getSupportFragmentManager().beginTransaction();
                    switch (item.getItemId()) {
                        case R.id.home:
                            searchButton.setVisibility(View.VISIBLE);
                            Toast.makeText(getApplicationContext(), "SELECTED HOME", Toast.LENGTH_SHORT).show();
                            title.setText(R.string.home);
                            fT2.show(homeFragment);
                            fT2.hide(profileFragment);
                            fT2.hide(firstAidFragment);
                            fT2.hide(mapFragment);
                            fT2.hide(aqiFragment);
                            fT2.commit();
                            break;
                        case R.id.map:
                            searchButton.setVisibility(View.VISIBLE);
                            Toast.makeText(getApplicationContext(), "SELECTED MAP", Toast.LENGTH_SHORT).show();
                            title.setText(R.string.map);
                            fT2.show(mapFragment);
                            fT2.hide(profileFragment);
                            fT2.hide(firstAidFragment);
                            fT2.hide(homeFragment);
                            fT2.hide(aqiFragment);
                            fT2.commit();
                            break;
                        case R.id.history:
                            searchButton.setVisibility(View.GONE);
                            Toast.makeText(getApplicationContext(), "SELECTED HISTORY", Toast.LENGTH_SHORT).show();
                            title.setText(R.string.first_aid);
                            fT2.show(firstAidFragment);
                            fT2.hide(profileFragment);
                            fT2.hide(homeFragment);
                            fT2.hide(mapFragment);
                            fT2.hide(aqiFragment);
                            fT2.commit();
                            break;
                        case R.id.aqi:
                            searchButton.setVisibility(View.GONE);
                            title.setText(R.string.air_quality);
                            fT2.show(aqiFragment);
                            fT2.hide(profileFragment);
                            fT2.hide(homeFragment);
                            fT2.hide(mapFragment);
                            fT2.hide(firstAidFragment);
                            fT2.commit();
                            break;
                        case R.id.profile:
                            searchButton.setVisibility(View.GONE);
                            Toast.makeText(getApplicationContext(), "SELECTED PROFILE", Toast.LENGTH_SHORT).show();
                            title.setText(R.string.profile);
                            fT2.show(profileFragment);
                            fT2.hide(homeFragment);
                            fT2.hide(firstAidFragment);
                            fT2.hide(mapFragment);
                            fT2.hide(aqiFragment);
                            fT2.commit();
                            break;
                    }

                    return true;
                }
            };

    private BottomNavigationView.OnNavigationItemReselectedListener navigationItemReselectedListener = new BottomNavigationView.OnNavigationItemReselectedListener() {
        @Override
        public void onNavigationItemReselected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.home:
                    Toast.makeText(getApplicationContext(), "loasdlasdasdao", Toast.LENGTH_SHORT).show();
                    scrollListener.scrollToTop();
                    break;
                case R.id.history:
                    break;
            }

        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                assert data != null;
                clinicReturn = data.getParcelableExtra("clinic_return");
                zoomOnce = true;
//                assert clinicResult != null;
//                Toast.makeText(this, clinicResult.getName() + "", Toast.LENGTH_SHORT).show();
//
//                Fragment oldMapFragment = getSupportFragmentManager().findFragmentByTag("mapFragment");
//                if (oldMapFragment != null) {
//                    Toast.makeText(this, "cant", Toast.LENGTH_SHORT).show();
//                    getSupportFragmentManager().beginTransaction().remove(oldMapFragment).commit();
//                }
//
//                Bundle bundle = new Bundle();
//                bundle.putInt("clinicID", clinicResult.getID());
//                bundle.putParcelable("clinic", clinicResult);
//                mapFragment = new MapFragment();
//                mapFragment.setArguments(bundle);
                MapFragment mapFragment = (MapFragment) getSupportFragmentManager().findFragmentByTag("mapFragment");
                assert mapFragment != null;
                mapFragment.drawWayInActivity(currentLatLng, clinicReturn);

                loadingDialog = findViewById(R.id.loadingDialog);
                loadingDialog.setVisibility(View.VISIBLE);
                title.setText(R.string.map);
                BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);
                bottomNavigationView.getMenu().findItem(R.id.map).setChecked(true);
                getSupportFragmentManager().beginTransaction().show(mapFragment).hide(homeFragment).commit();
//
//                BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);
//                bottomNavigationView.getMenu().findItem(R.id.map).setChecked(true);
//                getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, mapFragment, "mapFragment")
//                        .show(mapFragment).hide(homeFragment).commit();

            }
        }
    }

    public Clinic getClinic() {
        return clinicReturn;
    }

    public void setListener(ScrollInterface listener)
    {
        this.scrollListener = listener ;
    }

    private void updateUserWithName(String email) {
        final FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();
        final User user = new User("", email , "", "", "user");
        assert firebaseUser != null;

        Log.i("USERID",firebaseUser.getUid());
        final DatabaseReference userRef = mDatabaseReference.child("users").child(firebaseUser.getUid());
        userRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (!snapshot.exists()) {
                    userRef.setValue(user);
                } else {
                    Toast.makeText(HomeActivity.this, "LUL ALREADY HAD", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
    }

    private Task<String> fetchData() {
        Log.i("FETCH","FETCH");
        // Create the arguments to the callable function.
        final Map<String, Object> data = new HashMap<>();

        return mFunctions
                .getHttpsCallable("getAllHospitals")
                .call(data)
                .continueWith(new Continuation<HttpsCallableResult, String>() {
                    @Override
                    public String then(@NonNull Task<HttpsCallableResult> task) throws Exception {
                        // This continuation runs on either success or failure, but if the task
                        // has failed then getResult() will throw an Exception which will be
                        // propagated down.
                        Gson gson = new Gson();
                        if (task.getResult() != null) {
                            String json = gson.toJson(task.getResult().getData());
                            JSONArray jsonArray = new JSONArray(json);

                            for (int i = 0; i <jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                Log.i("VuTST",jsonObject.getString("name"));
                                mClinicRoomDatabase.clinicDao().insert(new Clinic(Integer.parseInt(jsonObject.getString("id")),
                                        jsonObject.getString("name"), jsonObject.getString("address"),
                                        jsonObject.getString("imageURL"),
                                        Double.parseDouble(jsonObject.getString("rating")),
                                        Double.parseDouble(jsonObject.getString("lat")),
                                        Double.parseDouble(jsonObject.getString("lng")),
                                        jsonObject.getString("phone"),jsonObject.getString("type"),jsonObject.getString("eng_name"),"","","","","","",""));
                            }
                        }

                        assert task.getResult() != null;
                        assert task.getResult().getData() !=null;

                        return task.getResult().getData().toString();
                    }
                });
    }


    private Task<String> getOpeningHours() {
        Log.i("FETCH","FETCH");
        // Create the arguments to the callable function.
        final Map<String, Object> data = new HashMap<>();

        return mFunctions
                .getHttpsCallable("getOpeningHours")
                .call(data)
                .continueWith(  new Continuation<HttpsCallableResult, String>() {
                    @Override
                    public String then(@NonNull Task<HttpsCallableResult> task) throws Exception {
                        // This continuation runs on either success or failure, but if the task
                        // has failed then getResult() will throw an Exception which will be
                        // propagated down.
                        Gson gson = new Gson();
                        if (task.getResult() != null) {
                            String json = gson.toJson(task.getResult().getData());
                            JSONArray jsonArray = new JSONArray(json);

                            Log.i("VUTST",jsonArray.toString());
                            for (int i = 0; i <jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                if (jsonObject.length() != 2) {
                                    mClinicRoomDatabase.clinicDao().updateOpeningHours(jsonObject.getString("monday"),
                                            jsonObject.getString("tuesday"),jsonObject.getString("wednesday"),
                                            jsonObject.getString("thursday"),jsonObject.getString("friday"),
                                            jsonObject.getString("saturday"),jsonObject.getString("sunday"),
                                            Integer.valueOf(jsonObject.getString("hospital_id")));
                                } else {
                                    mClinicRoomDatabase.clinicDao().updateOpeningHours("Monday: 7:30 AM - 4:30 PM",
                                            "Tuesday: 7:30 AM - 4:30 PM",
                                            "Wednesday: 7:30 AM - 4:30 PM",
                                            "Thursday: 7:30 AM - 4:30 PM",
                                            "Friday: 7:30 AM - 4:30 PM",
                                            "Saturday: 7:30 AM - 4:30 PM",
                                            "Sunday: 7:30 AM - 4:30 PM",
                                            Integer.valueOf(jsonObject.getString("hospital_id")));

                                }


                            }
                        }

                        assert task.getResult() != null;
                        assert task.getResult().getData() !=null;

                        return task.getResult().getData().toString();
                    }
                });
    }

    public void openingHourListener() {
        getOpeningHours()
                .addOnCompleteListener(new OnCompleteListener<String>() {
                    @Override
                    public void onComplete(@NonNull Task<String> task) {
                        if (!task.isSuccessful()) {
                            Exception e = task.getException();
                            if (e instanceof FirebaseFunctionsException) {
                                FirebaseFunctionsException ffe = (FirebaseFunctionsException) e;
                                FirebaseFunctionsException.Code code = ffe.getCode();
                                Object details = ffe.getDetails();
                                assert details != null;
                                Log.w("VuTST", code.toString() + details.toString());
                            }

                            Log.w("VuTST", "getHours:onFailure", e);

                        }
                        String result = task.getResult();
                        Log.i("VutST",result+"");

                    }
                });
    }
    public void fetch() {
        fetchData()
                .addOnCompleteListener(new OnCompleteListener<String>() {
                    @Override
                    public void onComplete(@NonNull Task<String> task) {
                        if (!task.isSuccessful()) {
                            Exception e = task.getException();
                            if (e instanceof FirebaseFunctionsException) {
                                FirebaseFunctionsException ffe = (FirebaseFunctionsException) e;
                                FirebaseFunctionsException.Code code = ffe.getCode();
                                Object details = ffe.getDetails();
                                assert details != null;
                                Log.w("VuTST", code.toString() + details.toString());
                            }

                            Log.w("VuTST", "getHospitals:onFailure", e);

                        }
                        openingHourListener();
                        String result = task.getResult();
                        Log.i("VutST",result+"");

                    }
                });
    }



    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setMessage(R.string.exit_warning)
                .setPositiveButton(R.string.exit_yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finishAffinity();
                    }
                })
                .setNegativeButton(R.string.exit_no,null)
                .show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "The activity is dead", Toast.LENGTH_SHORT).show();
    }
}
