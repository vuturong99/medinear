package com.example.medinear.view.activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.medinear.R;
import com.example.medinear.model.Accident;

public class DetailFirstAIdActivity extends AppCompatActivity {

    TextView title;
    ImageView backButton;
    Accident accident;
    TextView guides;
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_first_aid);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        init();
        loadActionBar();
        getData();
    }

    private void init() {
        guides = findViewById(R.id.guides);
    }

    private void getData() {
        if (getIntent() != null) {
            accident = getIntent().getParcelableExtra("accident");
            assert accident != null;
            guides.setText(accident.getGuidelines());
            title.setText(accident.getName());
        }
    }

    private void loadActionBar() {
        if (this.getSupportActionBar() != null) {
            this.getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setCustomView(R.layout.custom_action_bar);
            getSupportActionBar().setElevation(5);
            View view = getSupportActionBar().getCustomView();
            title = view.findViewById(R.id.title);
            backButton = view.findViewById(R.id.back_button);
            backButton.setOnClickListener(backListener);
        }
    }

    View.OnClickListener backListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            finish();
        }
    };


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


}
//Anaphylaxis (or anaphylactic shock) is a severe allergic reaction that can occur after an insect sting or after eating certain foods. The adverse reaction can be very fast, occurring within seconds or minutes of coming into contact with the substance the person is allergic to (allergen).\nDuring anaphylactic shock, it may be difficult for the person to breathe, as their tongue and throat may swell, obstructing their airway. \n\nCall 115 immediately if you think someone is experiencing anaphylactic shock. \n\nCheck if the person is carrying any medication. Some people who know they have severe allergies may carry an adrenaline self-injector, which is a type of pre-loaded syringe. You can either help the person administer their medication or, if you\'re trained to do so, give it to them yourself. \n\nAfter the injection, continue to look after the person until medical help arrives. All casualties who have had an intramuscular or subcutaneous (under the skin) injection of adrenaline must be seen and medically checked by a healthcare professional as soon as possible after the injection has been given. \n\nMake sure they\'re comfortable and can breathe as best they can while waiting for medical help to arrive. If they\'re conscious, sitting upright is normally the best position for them.