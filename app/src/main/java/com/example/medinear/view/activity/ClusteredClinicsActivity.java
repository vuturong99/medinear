package com.example.medinear.view.activity;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.medinear.R;
import com.example.medinear.model.Clinic;
import com.example.medinear.view.adapter.ClinicAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.example.medinear.view.activity.SearchActivity.RETURN_CODE_DETAIL;

public class ClusteredClinicsActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    ClinicAdapter clinicAdapter;
    List<Clinic> clinicList = new ArrayList<>();
    View view;
    TextView title;
    ImageView backButton;
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        setContentView(R.layout.activity_clustered_clinics);
        getData();
        initView();
        loadActionBar();
    }

    private void getData() {
        clinicList = getIntent().getParcelableArrayListExtra("clinics");

    }

    private void initView() {
        recyclerView = findViewById(R.id.list_result);
        clinicAdapter = new ClinicAdapter(clinicList, mItemClickListener, getApplicationContext());
        recyclerView.setAdapter(clinicAdapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (data != null) {
                Clinic clinic = data.getParcelableExtra("clinic_return");
                Intent clinicIntent = new Intent();
                clinicIntent.putExtra("clinic_return", clinic);
                Log.i("VCLThE",clinic.getName());
                setResult(Activity.RESULT_OK, clinicIntent);
                finish();
            }
        }

    }

    private final ClinicAdapter.ItemClickListener mItemClickListener = new ClinicAdapter.ItemClickListener() {
        @Override
        public void onClick(Clinic clinic) {
            Log.i("HomeFragment", "Clinic details " + clinic.getName() + " " + clinic.getAddress());
            Intent intent = new Intent(getApplicationContext(), DetailActivity.class);
            intent.putExtra("clinic", clinic);
            startActivityForResult(intent, 1);
        }
    };

    private void loadActionBar() {
        if (this.getSupportActionBar() != null) {
            this.getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setCustomView(R.layout.custom_action_bar);
            getSupportActionBar().setElevation(5);
            view = getSupportActionBar().getCustomView();
            title = view.findViewById(R.id.title);
            title.setText(String.format(Locale.getDefault(),"%d %s", clinicList.size(), getString(R.string.hospitals)));
            backButton = view.findViewById(R.id.back_button);
            backButton.setOnClickListener(backListener);
        }
    }

    View.OnClickListener backListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            finish();
        }
    };


}



