package com.example.medinear.view.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.example.medinear.R;
import com.example.medinear.model.Accident;
import com.example.medinear.view.activity.DetailFirstAIdActivity;
import com.example.medinear.view.adapter.FirstAidAdapter;
import com.example.medinear.viewmodel.FirstAidViewModel;

import java.util.ArrayList;
import java.util.List;

public class FirstAidFragment extends Fragment {

//    private ActionBarListener mActionBarListener = new ActionBarListener() {
//        @Override
//        public void onClickListener() {
//            Toast.makeText(getContext(), "ALO HISTORY", Toast.LENGTH_SHORT).show();
//
//        }
//    };

    List<Accident> accidentList = new ArrayList<>();
    FirstAidAdapter firstAidAdapter;
    RecyclerView recyclerView;
    FirstAidViewModel firstAidViewModel;
    EditText searchBar;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_first_aid, container, false);
        initViews(view);

        return view;

    }



    private void initViews(View view) {
        searchBar = view.findViewById(R.id.search_editText);
        firstAidViewModel = new ViewModelProvider(requireActivity()).get(FirstAidViewModel.class);
        recyclerView = view.findViewById(R.id.firstAidList);
        firstAidAdapter = new FirstAidAdapter(mItemClickListener, accidentList);
        searchBar.addTextChangedListener(textChangedListener);
        firstAidViewModel.getAllGuides().observe(getViewLifecycleOwner(), new Observer<List<Accident>>() {
            @Override
            public void onChanged(List<Accident> accidents) {
                firstAidAdapter.setFirstAid(accidents);
            }
        });

        recyclerView.setAdapter(firstAidAdapter);

    }


    private final FirstAidAdapter.ItemClickListener mItemClickListener = new FirstAidAdapter.ItemClickListener() {
        @Override
        public void onClick(Accident accident) {
            Toast.makeText(getContext(), "?", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(getContext(), DetailFirstAIdActivity.class);
            intent.putExtra("accident",accident);
            startActivity(intent);
        }
    };

    private TextWatcher textChangedListener = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            firstAidViewModel.getSearchedGudies(editable.toString()).observe(getViewLifecycleOwner(), new Observer<List<Accident>>() {
                @Override
                public void onChanged(List<Accident> accidents) {
                    firstAidAdapter.setFirstAid(accidents);
                }
            });
        }
    };

}
