package com.example.medinear.view.activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.medinear.R;
import com.example.medinear.model.HospitalType;
import com.example.medinear.view.adapter.HospitalTypeAdapter;

import java.util.ArrayList;
import java.util.List;

public class FilterActivity extends AppCompatActivity {

    ArrayList<HospitalType> hospitalTypeList;
    ArrayList<String> hospitalTypesTrue;
    HospitalTypeAdapter hospitalTypeAdapter;
    RecyclerView recyclerView;
    TextView sortByRatings;
    TextView sortAlphaDesc;
    TextView sortAlphaAsc;
    TextView title;
    ImageView backButton;
    Button applyButton;
    CheckBox checkBox;
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        loadActionBar();
        initFilterList();
        init();
    }

    private void loadActionBar() {
        if (this.getSupportActionBar() != null) {
            this.getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setCustomView(R.layout.custom_action_bar_text);
            getSupportActionBar().setElevation(5);
            View view = getSupportActionBar().getCustomView();
            title = view.findViewById(R.id.title);
            title.setText(R.string.filter);
            backButton = view.findViewById(R.id.back_button);
            backButton.setOnClickListener(backListener);
        }
    }

    private void init() {
        sortAlphaAsc = findViewById(R.id.sortAlphaAsc);
        sortAlphaDesc = findViewById(R.id.sortAlphaDesc);
        sortByRatings = findViewById(R.id.sortByRatings);
        applyButton = findViewById(R.id.applyButton);
        recyclerView = findViewById(R.id.hospitalTypeList);
        checkBox = findViewById(R.id.openClinic);

        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkBox.setSelected(true);
            }
        });

        sortAlphaAsc.setOnClickListener(onClickListenerAlphaAsc);
        sortByRatings.setOnClickListener(onClickListenerRate);
        sortAlphaDesc.setOnClickListener(onClickListenerAlphaDesc);

        applyButton.setOnClickListener(applyFilterListener);


        hospitalTypeAdapter = new HospitalTypeAdapter(hospitalTypeList,mItemClickListener);
        hospitalTypeAdapter.notifyDataSetChanged();
        recyclerView.setAdapter(hospitalTypeAdapter);
    }

    private void initFilterList() {
        hospitalTypesTrue = new ArrayList<>();
        hospitalTypeList = new ArrayList<>();
        hospitalTypeList.add(new HospitalType("General", getResources().getString(R.string.general),false));
        hospitalTypeList.add(new HospitalType("Dental",getString(R.string.type_dental),false));
        hospitalTypeList.add(new HospitalType("Lung",getString(R.string.type_lung),false));
        hospitalTypeList.add(new HospitalType("Obstetric and Gynaecology",getString(R.string.type_ong),false));
        hospitalTypeList.add(new HospitalType("Mental", getString(R.string.type_mental),false));
        hospitalTypeList.add(new HospitalType("Eye",getString(R.string.type_eye),false));
        hospitalTypeList.add(new HospitalType("Heart",getString(R.string.type_heart),false));
        hospitalTypeList.add(new HospitalType("ORL",getString(R.string.type_orl),false));
        hospitalTypeList.add(new HospitalType("Children",getString(R.string.type_children),false));
        hospitalTypeList.add(new HospitalType("Skin",getString(R.string.type_skin),false));
        hospitalTypeList.add(new HospitalType("Kidney",getString(R.string.type_kidney),false));
        hospitalTypeList.add(new HospitalType("Endocrinology",getString(R.string.type_endo),false));
        hospitalTypeList.add(new HospitalType("Beauty",getString(R.string.type_beauty),false));
        hospitalTypeList.add(new HospitalType("Onology",getString(R.string.type_onology),false));
        hospitalTypeList.add(new HospitalType("Traditional medicine",getString(R.string.type_tradition),false));
        hospitalTypeList.add(new HospitalType("Geriatric",getString(R.string.type_geriatric),false));
        hospitalTypeList.add(new HospitalType("Burn",getString(R.string.type_burn),false));
    }

    private final HospitalTypeAdapter.ItemClickListener mItemClickListener = new HospitalTypeAdapter.ItemClickListener() {
        @Override
        public void onClick(HospitalType hospitalType) {
            hospitalType.setChecked(!hospitalType.isChecked());
            hospitalTypeAdapter.notifyDataSetChanged();
            hospitalTypesTrue.add(hospitalType.getType());
        }

    };

    View.OnClickListener applyFilterListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent filterIntent = new Intent();
            Log.i("TSTV", sortAlphaAsc.isSelected() + "");
            Log.i("TSTV", sortByRatings.isSelected() + "");
            Log.i("TSTV", sortAlphaDesc.isSelected() + "");
            Log.i("TSTV", checkBox.isSelected() + "");
            filterIntent.putExtra("filter_sortAlphaDesc",sortAlphaDesc.isSelected());
            filterIntent.putExtra("filter_sortAlphaAsc",sortAlphaAsc.isSelected());
            filterIntent.putExtra("filter_sortRating",sortByRatings.isSelected());
            filterIntent.putExtra("filter_openClinics",checkBox.isSelected());
            filterIntent.putStringArrayListExtra("filter_type",hospitalTypesTrue);
            setResult(Activity.RESULT_OK, filterIntent);
            finish();
        }
    };
    View.OnClickListener onClickListenerRate = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            boolean isSelected = !view.isSelected();
            view.setSelected(isSelected);

            if (isSelected) {
                sortAlphaAsc.setSelected(false);
                sortAlphaAsc.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                sortAlphaDesc.setSelected(false);
                sortAlphaDesc.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                view.setBackgroundColor(getResources().getColor(R.color.light_grey));
            } else {
                view.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            }
        }
    };

    View.OnClickListener onClickListenerAlphaAsc = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            boolean isSelected = !view.isSelected();
            view.setSelected(isSelected);

            if (isSelected) {
                sortByRatings.setSelected(false);
                sortByRatings.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                sortAlphaDesc.setSelected(false);
                sortAlphaDesc.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                view.setBackgroundColor(getResources().getColor(R.color.light_grey));
            } else {
                view.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            }
        }
    };

    View.OnClickListener onClickListenerAlphaDesc = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            boolean isSelected = !view.isSelected();
            view.setSelected(isSelected);

            if (isSelected) {
                sortAlphaAsc.setSelected(false);
                sortAlphaAsc.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                sortByRatings.setSelected(false);
                sortByRatings.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                view.setBackgroundColor(getResources().getColor(R.color.light_grey));
            } else {
                view.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            }
        }
    };

    View.OnClickListener backListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            finish();
        }
    };


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
