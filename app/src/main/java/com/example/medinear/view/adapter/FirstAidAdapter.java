package com.example.medinear.view.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.example.medinear.R;
import com.example.medinear.model.Accident;
import com.example.medinear.model.HospitalType;
import com.squareup.picasso.Picasso;

import java.util.List;

public class FirstAidAdapter extends RecyclerView.Adapter<FirstAidAdapter.ViewHolder>{

    ItemClickListener itemClickListener;
    List<Accident> accidentList;

    public FirstAidAdapter(ItemClickListener itemClickListener, List<Accident> accidentList) {
        this.itemClickListener = itemClickListener;
        this.accidentList = accidentList;
    }

    public void setFirstAid(List<Accident> accidentList) {
        this.accidentList = accidentList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_first_aid, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Accident accident = accidentList.get(position);

        holder.accidentName.setText(accident.getName());
        holder.description.setText(accident.getDescription());
//        holder.image.setBackgroundResource(R.drawable.ic_dizziness);
        Picasso picasso = Picasso.get();
        picasso.setIndicatorsEnabled(false);
        picasso.load(accident.getImageURL()).into(holder.image);

    }

    @Override
    public int getItemCount() {
        return accidentList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView accidentName;
        TextView description;
        ImageView image;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            accidentName = itemView.findViewById(R.id.accident_name);
            description = itemView.findViewById(R.id.description);
            image = itemView.findViewById(R.id.first_aid_icon);
            View.OnClickListener itemClick = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemClickListener.onClick(accidentList.get(getAdapterPosition()));
                }
            };

            itemView.setOnClickListener(itemClick);

        }
    }

    public interface ItemClickListener {
        void onClick(Accident accident);
    }
}
