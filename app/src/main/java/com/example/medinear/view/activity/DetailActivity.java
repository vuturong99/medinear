package com.example.medinear.view.activity;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.medinear.R;
import com.example.medinear.database.ClinicRoomDatabase;
import com.example.medinear.model.Clinic;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;

public class DetailActivity extends AppCompatActivity {

    DecimalFormat decimalFormat;
    private Clinic mClinic;
    private TextView mCName;
    private TextView mCAdd;
    private ImageView mCUI;
    private TextView mCPhone;
    private Button mCallButton;
    private Button mNavigateButton;
    TextView mondayTime;
    TextView tuesdayTime;
    TextView wednesdayTime;
    TextView thursdayTime;
    TextView fridayTime;
    TextView saturdayTime;
    TextView sundayTime;
    MaterialRatingBar ratingBar;
    MaterialRatingBar viewRatingBar;
    TextView rateNumber;
    TextView clinicStatus;
    TextView clinicType;
    Button rateButton;
    View view;
    String mon, tue, wed, thu, fri, sat, sun;
    ClinicRoomDatabase mClinicRoomDatabase;
    DatabaseReference mClinicReference;
    DatabaseReference mRatingReference;
    DatabaseReference mRatingCountReference;
    FirebaseUser firebaseUser;
    ImageView backButton;
    private final View.OnClickListener mCallButtonClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + mClinic.getPhone()));
            startActivity(intent);
        }
    };

    private final View.OnClickListener mNavigateButtonClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent clinicIntent = new Intent();
            clinicIntent.putExtra("clinic_return", mClinic);
            setResult(Activity.RESULT_OK, clinicIntent);
            finish();
        }
    };


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_detail);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        initView();
        loadActionBar();
        initDatabase();
        getData();
        getRatings();
        call();
        navigate();
    }

    private void loadActionBar() {
        if (this.getSupportActionBar() != null) {
            this.getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setCustomView(R.layout.custom_action_bar);
            getSupportActionBar().setElevation(5);
            view = getSupportActionBar().getCustomView();
            TextView title = view.findViewById(R.id.title);
            backButton = view.findViewById(R.id.back_button);
            backButton.setOnClickListener(backListener);
            title.setText(R.string.details);
        }
    }


//    private void initViewModel() {
//        clinicAdapterViewModel = new ViewModelProvider(this).get(ClinicAdapterViewModel.class);
//        clinicAdapterViewModel.getClinicAdapter().observe(this, new Observer<ClinicAdapter>() {
//            @Override
//            public void onChanged(ClinicAdapter clinicAdapter) {
//                mClinicAdapter = clinicAdapter;
//            }
//         });
//    }

    private void getRatings() {
        DatabaseReference ratingRef = mRatingReference.child(firebaseUser.getUid()).child(String.valueOf(mClinic.getID()));
        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.getValue() != null) {
                    ratingBar.setRating(Float.valueOf(snapshot.getValue().toString()));
                } else {
                    ratingBar.setRating(0.0f);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        };

        ratingRef.addValueEventListener(valueEventListener);
    }

    private void navigate() {
        mNavigateButton.setOnClickListener(mNavigateButtonClick);
    }

    private void call() {
        mCallButton.setOnClickListener(mCallButtonClick);
    }

    private void initDatabase() {
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        mRatingReference = FirebaseDatabase.getInstance().getReference().child("ratings");
        mClinicReference = FirebaseDatabase.getInstance().getReference().child("hospitals");
        mClinicRoomDatabase = ClinicRoomDatabase.getDatabase(getApplicationContext());

    }


    private void initView() {
        ratingBar = findViewById(R.id.ratingBar);
        rateButton = findViewById(R.id.rateButton);
        rateButton.setOnClickListener(rateListener);
        mCName = findViewById(R.id.detailed_clinicName);
        mCAdd = findViewById(R.id.detailed_clinicAddress);
        mCUI = findViewById(R.id.detailed_clinicImage);
        mondayTime = findViewById(R.id.monday_time);
        tuesdayTime = findViewById(R.id.tuesday_time);
        wednesdayTime = findViewById(R.id.wednesday_time);
        thursdayTime = findViewById(R.id.thursday_time);
        fridayTime = findViewById(R.id.friday_time);
        saturdayTime = findViewById(R.id.saturday_time);
        sundayTime = findViewById(R.id.sunday_time);
        viewRatingBar = findViewById(R.id.clinicRating);
        rateNumber = findViewById(R.id.rateNumber);
        clinicStatus = findViewById(R.id.clinic_status);
        clinicType = findViewById(R.id.clinic_type);

//        mCOH = findViewById(R.id.open);
        mCPhone = findViewById(R.id.detailed_clinicPhone);
        mCallButton = findViewById(R.id.callButton);
        mNavigateButton = findViewById(R.id.navigateButton);
    }

    View.OnClickListener rateListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            double rate = ratingBar.getRating();
            rate(mClinic.getID(), rate);

        }
    };

    private void rate(final int id, final double rate) {
        final Clinic clinic = mClinicRoomDatabase.clinicDao().getClinicByID(id);
        mRatingCountReference = FirebaseDatabase.getInstance().getReference().child("rating_count").
                child(String.valueOf(clinic.getID())).child("rating_count");

        mRatingCountReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                assert snapshot.getValue() != null;

                double newRateValue = (rate + (clinic.getRating() * Integer.valueOf(snapshot.getValue().toString())) )
                                / (Integer.valueOf(snapshot.getValue().toString()) + 1);

                mClinicReference.child(String.valueOf(id)).child("rating").
                        setValue(newRateValue);

                mRatingReference.child(firebaseUser.getUid()).child(String.valueOf(clinic.getID())).setValue(rate);
                mClinicRoomDatabase.clinicDao().updateRating(newRateValue, id);

                mRatingCountReference.setValue(Integer.valueOf(snapshot.getValue().toString()) + 1);
                viewRatingBar.setRating((float) (newRateValue));
                rateNumber.setText(String.valueOf(newRateValue));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });






    }

    private void getData() {
        decimalFormat = new DecimalFormat("0.0000000");
        mClinic = getIntent().getParcelableExtra("clinic");
        if (mClinic != null) {
            mCName.setText(mClinic.getName());
            mCAdd.setText(mClinic.getAddress());
            Picasso.get().load(mClinic.getImageURL()).into(mCUI);
            Log.i("AAA", mClinic.getLatitude() + " " + mClinic.getLongitude());
            mCPhone.setText(mClinic.getPhone());

            //Time
            initSpans();
            timeMaster(mClinic);
            viewRatingBar.setRating((float) (mClinic.getRating()));
            rateNumber.setText(String.format(Locale.getDefault(), "%.2f", mClinic.getRating()));

            clinicType.setText(mClinic.getType());

        }
    }

    private void initSpans() {
        StringBuffer newMon, newTue, newWed, newThu, newFri, newSat, newSun;
        mon = mClinic.getMonday_time();
        tue = mClinic.getTuesday_time();
        wed = mClinic.getWednesday_time();
        thu = mClinic.getThursday_time();
        fri = mClinic.getFriday_time();
        sat = mClinic.getSaturday_time();
        sun = mClinic.getSunday_time();
        final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);

        newMon = new StringBuffer(mon);
        String[] monString = mon.split(" ");
        if (monString.length < 7) {
            newMon.insert(7, "\n\t\t\t");
        } else {
            newMon.insert(7, "\n\t\t\t");
            newMon.insert(mon.indexOf((",")) + 5, "\n\t\t\t");
        }
        Spannable monSpan = new SpannableString(newMon);
        monSpan.setSpan(bss, 0, 7, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mondayTime.setText(monSpan);

        newTue = new StringBuffer(tue);
        String[] tueString = tue.split(" ");
        if (tueString.length < 7) {
            newTue.insert(8, "\n\t\t\t");
        } else {
            newTue.insert(8, "\n\t\t\t");
            newTue.insert(tue.indexOf(",") + 5, "\n\t\t\t");
        }
        Spannable tueSpan = new SpannableString(newTue);
        tueSpan.setSpan(bss, 0, 8, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        tuesdayTime.setText(tueSpan);

        newWed = new StringBuffer(wed);
        String[] wedString = wed.split(" ");
        if (wedString.length < 7) {
            newWed.insert(10, "\n\t\t\t");
        } else {
            newWed.insert(10, "\n\t\t\t");
            newWed.insert(wed.indexOf(",") + 5, "\n\t\t\t");
        }
        Spannable wedSpan = new SpannableString(newWed);
        wedSpan.setSpan(bss, 0, 10, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        wednesdayTime.setText(wedSpan);

        newThu = new StringBuffer(thu);
        String[] thuString = thu.split(" ");
        if (thuString.length < 7) {
            newThu.insert(9, "\n\t\t\t");
        } else {
            newThu.insert(9, "\n\t\t\t");
            newThu.insert(thu.indexOf(",") + 5, "\n\t\t\t");
        }
        Spannable thuSpan = new SpannableString(newThu);
        thuSpan.setSpan(bss, 0, 9, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        thursdayTime.setText(thuSpan);

        newFri = new StringBuffer(fri);
        String[] friString = fri.split(" ");
        if (friString.length < 7) {
            newFri.insert(7, "\n\t\t\t");
        } else {
            newFri.insert(7, "\n\t\t\t");
            newFri.insert(fri.indexOf(",") + 5, "\n\t\t\t");
        }
        Spannable friSpan = new SpannableString(newFri);
        friSpan.setSpan(bss, 0, 7, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        fridayTime.setText(friSpan);

        newSat = new StringBuffer(sat);
        String[] satString = sat.split(" ");
        if (satString.length < 7) {
            newSat.insert(9, "\n\t\t\t");
        } else {
            newSat.insert(9, "\n\t\t\t");
            newSat.insert(sat.indexOf(",") + 5, "\n\t\t\t");
        }
        Spannable satSpan = new SpannableString(newSat);
        satSpan.setSpan(bss, 0, 9, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        saturdayTime.setText(satSpan);

        newSun = new StringBuffer(sun);
        String[] sunString = sun.split(" ");
        if (sunString.length < 7) {
            newSun.insert(7, "\n\t\t\t");
        } else {
            newSun.insert(7, "\n\t\t\t");
            newSun.insert(sun.indexOf(",") + 5, "\n\t\t\t");
        }
        Spannable sunSpan = new SpannableString(newSun);
        sunSpan.setSpan(bss, 0, 7, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        sundayTime.setText(sunSpan);


    }

    public void timeMaster(Clinic clinic) {
        String getTime;
        Date now = new Date();
        Date time1;
        Date time2;
        String timeNow;


        SimpleDateFormat simpleDateformat = new SimpleDateFormat("E", Locale.US);
        SimpleDateFormat simpleTimeFormat = new SimpleDateFormat("hh:mm a", Locale.US);
        switch (simpleDateformat.format(now)) {
            case "Mon":
                getTime = clinic.getMonday_time();
                break;
            case "Tue":
                getTime = clinic.getTuesday_time();
                break;
            case "Wed":
                getTime = clinic.getWednesday_time();
                break;
            case "Thu":
                getTime = clinic.getThursday_time();
                break;
            case "Fri":
                getTime = clinic.getFriday_time();
                break;
            case "Sat":
                getTime = clinic.getSaturday_time();
                break;
            case "Sun":
                getTime = clinic.getSunday_time();
                break;
            default:
                getTime = "Open: 7:30 AM - Close: 4:30 PM";
        }

        if (getTime.contains("24")) {
            clinicStatus.setTextColor(getApplication().getResources().getColor(R.color.darkGreen));
            clinicStatus.setText(R.string.open);
        } else if (getTime.contains("Closed")) {
            clinicStatus.setTextColor(getApplication().getResources().getColor(R.color.darkRed));
            clinicStatus.setText(R.string.closed);
        } else {
            String[] timeStrings = getTime.split(" ");

            try {
                time1 = simpleTimeFormat.parse(timeStrings[1] + " " + timeStrings[2]);
                time2 = simpleTimeFormat.parse(timeStrings[timeStrings.length - 2] + " " + timeStrings[timeStrings.length - 1]);
                timeNow = simpleTimeFormat.format(new Date());
                now = simpleTimeFormat.parse(timeNow);
                Log.i("TIME", now + "");
                Log.i("TIME", timeNow);

                if (time1.before(now) && time2.after(now)) {
                    clinicStatus.setTextColor(getApplication().getResources().getColor(R.color.darkGreen));
                    clinicStatus.setText(R.string.open);
                } else {
                    clinicStatus.setTextColor(getApplication().getResources().getColor(R.color.darkRed));
                    clinicStatus.setText(R.string.closed);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

    }


    View.OnClickListener backListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            finish();
        }
    };
}
