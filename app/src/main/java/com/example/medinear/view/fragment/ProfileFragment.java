package com.example.medinear.view.fragment;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.medinear.R;
import com.example.medinear.view.activity.AboutActivity;
import com.example.medinear.view.activity.EditProfileActivity;
import com.example.medinear.view.activity.LoginActivity;

import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.auth.TwitterAuthProvider;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.IOException;


public class ProfileFragment extends Fragment {

    private GoogleSignInClient mGoogleSignInClient;
    private FirebaseAuth firebaseAuth;
    private TextView name;
    private TextView ephone;
    private TextView address;
    private ImageView avatar;
    private ImageView banner;
    private String facebookUserId;
    DatabaseReference userRef;
    FirebaseUser firebaseUser;
    Button aboutButton;
    private String twitterUserId;
    private String googleUserId;
    private static final int PICK_AVATAR= 2;
    private static final int PICK_BANNER = 3;





    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        firebaseAuth = FirebaseAuth.getInstance();
        mGoogleSignInClient = GoogleSignIn.getClient(requireActivity(),gso);
        getUserInfo();

        initViews(view);


        checkUserAvatar();
        checkUserBanner();
        return view;
    }

    private void checkUserAvatar() {
       StorageReference storageReference = FirebaseStorage.getInstance().getReference()
                .child("user_images")
                .child("user_avatars")
                .child(firebaseUser.getUid())
                .child("avatar.jpeg");

       storageReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
           @Override
           public void onSuccess(Uri uri) {
               Picasso.get().load(uri).into(avatar);

               Log.i("ERRORR",uri + "");

           }
       }).addOnFailureListener(new OnFailureListener() {
           @Override
           public void onFailure(@NonNull Exception e) {
               Log.i("ERRORR",e.getCause() + "");
           }
       });
    }

    private void checkUserBanner() {
        StorageReference storageReference = FirebaseStorage.getInstance().getReference()
                .child("user_images")
                .child("user_banners")
                .child(firebaseUser.getUid())
                .child("banner.jpeg");

        storageReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                Picasso.get().load(uri).into(banner);

                Log.i("ERRORR",uri + "");

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.i("ERRORR",e.getCause() + "");
            }
        });
    }

    private void getUserInfo() {
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        userRef = FirebaseDatabase.getInstance().getReference().child("users").child(firebaseUser.getUid());
        DatabaseReference nameRef = userRef.child("name");
        DatabaseReference ephoneRef = userRef.child("emergency_contact");
        DatabaseReference addressRef = userRef.child("address");

        nameRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                assert snapshot.getValue() != null;
                name.setText(snapshot.getValue().toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        ephoneRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                assert snapshot.getValue() != null;
                ephone.setText(snapshot.getValue().toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        addressRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                assert snapshot.getValue() != null;
                address.setText(snapshot.getValue().toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void initViews(View view) {
        aboutButton = view.findViewById(R.id.about);
        aboutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), AboutActivity.class));
            }
        });
        name = view.findViewById(R.id.name);
        ephone = view.findViewById(R.id.ephone);
        address = view.findViewById(R.id.address);
        ImageView setAvatar = view.findViewById(R.id.changeAvatarButton);
        ImageView setBanner = view.findViewById(R.id.changeBannerButton);
        banner = view.findViewById(R.id.banner);
        avatar = view.findViewById(R.id.avatar);

        if (firebaseAuth.getCurrentUser() != null) {
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            assert user != null;
            for(UserInfo profile : user.getProviderData()) {
                // check if the provider id matches "facebook.com"
                if (TwitterAuthProvider.PROVIDER_ID.equals(profile.getProviderId())) {
                    Picasso.get().load(profile.getPhotoUrl()).into(avatar);
                } else
                if(FacebookAuthProvider.PROVIDER_ID.equals(profile.getProviderId())) {
                    facebookUserId = profile.getUid();
                    String photoUrl = "https://graph.facebook.com/" + facebookUserId + "/picture?height=500";
                    Picasso.get().load(photoUrl).into(avatar);

//                    String coverUrl = "http://graph.facebook.com/" + facebookUserId + "?fields=cover";
//                    Log.i("FACEBOOK",coverUrl);
                }
                else if (GoogleAuthProvider.PROVIDER_ID.equals(profile.getProviderId())) {
                    Picasso.get().load(profile.getPhotoUrl()).into(avatar);
                }
            }


        }

        setAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                startActivityForResult(intent, PICK_AVATAR);
            }
        });

        setBanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                startActivityForResult(intent, PICK_BANNER);
            }
        });

        TextView emailTV = view.findViewById(R.id.p_email);

        assert firebaseAuth.getCurrentUser() != null;
        if (firebaseAuth.getCurrentUser().getEmail() != null) {
            if (!firebaseAuth.getCurrentUser().getEmail().equals("")) {
                String lol = firebaseAuth.getCurrentUser().getEmail();
                Log.i("TWITTER",lol);
                emailTV.setText(firebaseAuth.getCurrentUser().getEmail());
            } else {
                String lol2 = firebaseAuth.getCurrentUser().getDisplayName();
                Log.i("TWITTER",lol2+"");
                emailTV.setText(firebaseAuth.getCurrentUser().getDisplayName());
            }
        } else {
            String lol2 = firebaseAuth.getCurrentUser().getDisplayName();
            Log.i("TWITTER",lol2+"");
            emailTV.setText(firebaseAuth.getCurrentUser().getDisplayName());
        }

        Button editProfileBtn = view.findViewById(R.id.editProfile);

        editProfileBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), EditProfileActivity.class);
                intent.putExtra("name",name.getText().toString());
                intent.putExtra("ephone",ephone.getText().toString());
                intent.putExtra("address",address.getText().toString());
                startActivityForResult(intent, 1);
            }
        });

        Button logoutBtn = view.findViewById(R.id.logout);
        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                firebaseAuth.signOut();
                mGoogleSignInClient.signOut();
                LoginManager.getInstance().logOut();
                requireActivity().finish();
                startActivity(new Intent(getActivity(), LoginActivity.class));
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                assert data != null;
                String test = data.getStringExtra("name_return");
                Log.i("ASDASDASd",test+"");
                name.setText(data.getStringExtra("name_return"));
                ephone.setText(data.getStringExtra("ephone_return"));
                address.setText(data.getStringExtra("address_return"));

                ephone.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + ephone.getText().toString()));
                        startActivity(intent);
                    }
                });
            }
        }



        if (requestCode == PICK_AVATAR) {
            if (resultCode == Activity.RESULT_OK) {
                assert data != null;
                Uri uri = data.getData();
                Bitmap bitmap = null;
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(requireActivity().getContentResolver(), uri);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                assert bitmap != null;
                uploadAvatar(bitmap);
                Picasso.get().load(uri).into(avatar);
            }
        }

        if (requestCode == PICK_BANNER) {
            if (resultCode == Activity.RESULT_OK) {
                assert data != null;
                Uri uri = data.getData();
                Bitmap bitmap = null;
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(requireActivity().getContentResolver(), uri);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                assert bitmap != null;
                uploadBanner(bitmap);
                Picasso.get().load(uri).into(banner);
            }

        }
    }

    private void uploadBanner(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        final StorageReference storageReference = FirebaseStorage.getInstance().getReference()
                .child("user_images")
                .child("user_banners")
                .child(firebaseUser.getUid())
                .child("banner.jpeg");

        storageReference.putBytes(byteArrayOutputStream.toByteArray()).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                Toast.makeText(getContext(), "Updated banner", Toast.LENGTH_SHORT).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.i("ERRORRRR",e.getCause() + "");
            }
        });
    }

    private void uploadAvatar(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        final StorageReference storageReference = FirebaseStorage.getInstance().getReference()
                .child("user_images")
                .child("user_avatars")
                .child(firebaseUser.getUid())
                .child("avatar.jpeg");

        storageReference.putBytes(byteArrayOutputStream.toByteArray()).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                Toast.makeText(getContext(), "Updated avatar", Toast.LENGTH_SHORT).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.i("ERRORRRR",e.getCause() + "");
            }
        });

    }
}
