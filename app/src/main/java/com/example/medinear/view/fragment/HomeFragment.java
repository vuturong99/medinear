package com.example.medinear.view.fragment;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageButton;
import android.widget.TableLayout;
import android.widget.Toast;
import android.widget.ViewFlipper;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.medinear.MyRecyclerScroll;
import com.example.medinear.ScrollInterface;
import com.example.medinear.database.ClinicRoomDatabase;
import com.example.medinear.view.adapter.ClinicAdapter;
import com.example.medinear.viewmodel.ClinicViewModel;
import com.example.medinear.R;
import com.example.medinear.view.activity.DetailActivity;
import com.example.medinear.view.activity.HomeActivity;
import com.example.medinear.model.Clinic;
import com.example.medinear.viewmodel.LocationViewModel;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class HomeFragment extends Fragment implements ScrollInterface {

    //Recycler view, adapter, and view model
    private List<Clinic> mClinicList = new ArrayList<>();
    private ClinicViewModel mClinicViewModel;
    private ClinicAdapter mClinicAdapter;
    private RecyclerView mRecyclerView;
    private Observer<List<Clinic>> allObserver;
    private ViewFlipper mViewFlipper;
    private LatLng currentLatLng;
    private ImageButton generalF;
    private ImageButton lungF;
    private ImageButton heartF;
    private ImageButton dentistF;
    private ImageButton eyeF;
    private TableLayout filterTable;
    private CardView filterCardView;
    static final float MINIMUM = 25;
    int scrollDist = 0;


    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ScrollInterface mScrollListener = new ScrollInterface() {
        @Override
        public void scrollToTop() {
            Toast.makeText(getContext(), "heyyyyyyyyyyyyy", Toast.LENGTH_SHORT).show();
            mRecyclerView.smoothScrollToPosition(0);
        }
    };

    private final ClinicAdapter.ItemClickListener mItemClickListener = new ClinicAdapter.ItemClickListener() {
        @Override
        public void onClick(Clinic clinic) {
            Log.i("HomeFragment","Clinic details "+ clinic.getName() + " " + clinic.getAddress());
            Intent intent = new Intent(getContext(), DetailActivity.class);
            intent.putExtra("clinic",clinic);
            requireActivity().startActivityForResult(intent,1);
        }
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        initView(view);


        mClinicAdapter = new ClinicAdapter(mClinicList, mItemClickListener, getActivity());
        mRecyclerView.setAdapter(mClinicAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.addOnScrollListener(new MyRecyclerScroll() {
            @Override
            public void show() {
                filterCardView.animate().translationY(0);
            }

            @Override
            public void hide() {
                filterCardView.animate().translationY(filterCardView.getHeight() + 8);
            }
        });

        HomeActivity homeActivity = (HomeActivity) requireActivity();
        homeActivity.setListener(mScrollListener);
        setClickableImageViews(view);
        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        LocationViewModel viewModel = new ViewModelProvider(requireActivity()).get(LocationViewModel.class);
        viewModel.getCurrentLocation().observe(getViewLifecycleOwner(), new Observer<LatLng>() {
            @Override
            public void onChanged(LatLng latLng) {
                currentLatLng = latLng;
                mClinicAdapter.setCurrentLocation(latLng);
                mClinicAdapter.notifyDataSetChanged();
            }
        });
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        allObserver = new Observer<List<Clinic>>() {
            @Override
            public void onChanged(List<Clinic> clinics) {
                if (clinics.size() == 0) {
                    mViewFlipper.setDisplayedChild(0);
                } else {
                    mViewFlipper.setDisplayedChild(1);
                }

                mClinicAdapter.setClinics(clinics);
                mClinicAdapter.notifyDataSetChanged();
            }
        };
        mClinicViewModel.getMutableClinics().observe(getViewLifecycleOwner(), allObserver);
    }

    private void setClickableImageViews(View view) {
        //Clickable image views
        generalF = view.findViewById(R.id.generalF);
        lungF = view.findViewById(R.id.lungF);
        dentistF = view.findViewById(R.id.dentistF);
        heartF = view.findViewById(R.id.heartF);
        eyeF = view.findViewById(R.id.eyeF);


        generalF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                generalF.setSelected(!generalF.isSelected());
                lungF.setSelected(false);
                heartF.setSelected(false);
                dentistF.setSelected(false);
                eyeF.setSelected(false);

                if (generalF.isSelected()) {
                    mClinicViewModel.getMutableClinics().removeObserver(allObserver);
                    if (currentLatLng == null) {
                        Toast.makeText(getContext(), "Filtering in offline mode", Toast.LENGTH_SHORT).show();
                        mClinicViewModel.getClinicsByType("General").observe(getViewLifecycleOwner(), new Observer<List<Clinic>>() {
                            @Override
                            public void onChanged(List<Clinic> clinics) {
                                mViewFlipper.setDisplayedChild(1);
                                mClinicAdapter.setClinics(clinics);
                                mClinicAdapter.notifyDataSetChanged();
                            }
                        });
                    } else {
                        mClinicViewModel.getClinicsByType("General").observe(getViewLifecycleOwner(), new Observer<List<Clinic>>() {
                            @Override
                            public void onChanged(List<Clinic> clinics) {
                                mClinicAdapter.setClinics(returnClinics(currentLatLng, clinics));
                                mClinicAdapter.notifyDataSetChanged();
                            }
                        });
                    }



//                    mClinicViewModel.getClinics().observe(getViewLifecycleOwner(), new Observer<List<Clinic>>() {
//                        @Override
//                        public void onChanged(List<Clinic> clinics) {
//                            List<Clinic> filteredClinics = new ArrayList<>();
//                            for (Clinic clinic : clinics) {
//                                if (clinic.getType().equals("General")) {
//                                    filteredClinics.add(clinic);
//                                }
//                            }
//                            mClinicAdapter.setClinics(filteredClinics);
//                            mClinicAdapter.notifyDataSetChanged();
//                        }
//                    });
                } else {
//                    if (currentLatLng == null) {
//                        mClinicViewModel.getAllClinics().observe(getViewLifecycleOwner(), new Observer<List<Clinic>>() {
//                            @Override
//                            public void onChanged(List<Clinic> clinics) {
//                                mClinicAdapter.setClinics(clinics);
//                                mClinicAdapter.notifyDataSetChanged();
//                            }
//                        });
//                    } else {
                        mClinicViewModel.getMutableClinics().observe(getViewLifecycleOwner(), allObserver);


                }
            }
        });

        lungF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                generalF.setSelected(false);
                lungF.setSelected(!lungF.isSelected());
                heartF.setSelected(false);
                dentistF.setSelected(false);
                eyeF.setSelected(false);

                if (lungF.isSelected()) {
                    mClinicViewModel.getMutableClinics().removeObserver(allObserver);
                    if (currentLatLng == null) {
                        Toast.makeText(getContext(), "Filtering in offline mode", Toast.LENGTH_SHORT).show();
                        mClinicViewModel.getClinicsByType("Lung").observe(getViewLifecycleOwner(), new Observer<List<Clinic>>() {
                            @Override
                            public void onChanged(List<Clinic> clinics) {
                                mViewFlipper.setDisplayedChild(1);
                                mClinicAdapter.setClinics(clinics);
                                mClinicAdapter.notifyDataSetChanged();
                            }
                        });
                    } else {
                        mClinicViewModel.getClinicsByType("Lung").observe(getViewLifecycleOwner(), new Observer<List<Clinic>>() {
                            @Override
                            public void onChanged(List<Clinic> clinics) {
                                mClinicAdapter.setClinics(returnClinics(currentLatLng, clinics));
                                mClinicAdapter.notifyDataSetChanged();
                            }
                        });
                    }
                } else {
                    if (currentLatLng == null) {
                        mClinicViewModel.getMutableClinics().observe(getViewLifecycleOwner(), new Observer<List<Clinic>>() {
                            @Override
                            public void onChanged(List<Clinic> clinics) {
                                mClinicAdapter.setClinics(clinics);
                                mClinicAdapter.notifyDataSetChanged();
                            }
                        });
                    } else {
                        mClinicViewModel.getMutableClinics().observe(getViewLifecycleOwner(), allObserver);
                    }

                }
            }
        });

        dentistF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                generalF.setSelected(false);
                lungF.setSelected(false);
                heartF.setSelected(false);
                dentistF.setSelected(!dentistF.isSelected());
                eyeF.setSelected(false);

                if (dentistF.isSelected()) {
                    mClinicViewModel.getMutableClinics().removeObserver(allObserver);
                    if (currentLatLng == null) {
                        Toast.makeText(getContext(), "Filtering in offline mode", Toast.LENGTH_SHORT).show();
                        mClinicViewModel.getClinicsByType("Dental").observe(getViewLifecycleOwner(), new Observer<List<Clinic>>() {
                            @Override
                            public void onChanged(List<Clinic> clinics) {
                                mViewFlipper.setDisplayedChild(1);
                                mClinicAdapter.setClinics(clinics);
                                mClinicAdapter.notifyDataSetChanged();
                            }
                        });
                    } else {
                        mClinicViewModel.getClinicsByType("Dental").observe(getViewLifecycleOwner(), new Observer<List<Clinic>>() {
                            @Override
                            public void onChanged(List<Clinic> clinics) {
                                mClinicAdapter.setClinics(returnClinics(currentLatLng, clinics));
                                mClinicAdapter.notifyDataSetChanged();
                            }
                        });
                    }
                } else  {
                    if (currentLatLng == null) {
                        mClinicViewModel.getMutableClinics().observe(getViewLifecycleOwner(), new Observer<List<Clinic>>() {
                            @Override
                            public void onChanged(List<Clinic> clinics) {
                                mClinicAdapter.setClinics(clinics);
                                mClinicAdapter.notifyDataSetChanged();
                            }
                        });
                    } else {
                        mClinicViewModel.getMutableClinics().observe(getViewLifecycleOwner(), allObserver);
                    }
                }
            }
        });

        heartF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                generalF.setSelected(false);
                lungF.setSelected(false);
                heartF.setSelected(!heartF.isSelected());
                dentistF.setSelected(false);
                eyeF.setSelected(false);

                if (heartF.isSelected()) {
                    mClinicViewModel.getMutableClinics().removeObserver(allObserver);
                    if (currentLatLng == null) {
                        Toast.makeText(getContext(), "Filtering in offline mode", Toast.LENGTH_SHORT).show();
                        mClinicViewModel.getClinicsByType("Heart").observe(getViewLifecycleOwner(), new Observer<List<Clinic>>() {
                            @Override
                            public void onChanged(List<Clinic> clinics) {
                                mViewFlipper.setDisplayedChild(1);
                                mClinicAdapter.setClinics(clinics);
                                mClinicAdapter.notifyDataSetChanged();
                            }
                        });
                    } else {
                        mClinicViewModel.getClinicsByType("Heart").observe(getViewLifecycleOwner(), new Observer<List<Clinic>>() {
                            @Override
                            public void onChanged(List<Clinic> clinics) {
                                mClinicAdapter.setClinics(returnClinics(currentLatLng, clinics));
                                mClinicAdapter.notifyDataSetChanged();
                            }
                        });
                    }
                } else {
                    if (currentLatLng == null) {
                        mClinicViewModel.getMutableClinics().observe(getViewLifecycleOwner(), new Observer<List<Clinic>>() {
                            @Override
                            public void onChanged(List<Clinic> clinics) {
                                mClinicAdapter.setClinics(clinics);
                                mClinicAdapter.notifyDataSetChanged();
                            }
                        });
                    } else {
                        mClinicViewModel.getMutableClinics().observe(getViewLifecycleOwner(), allObserver);
                    }
                }
            }
        });

        eyeF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                generalF.setSelected(false);
                lungF.setSelected(false);
                heartF.setSelected(false);
                dentistF.setSelected(false);
                eyeF.setSelected(!eyeF.isSelected());

                if (eyeF.isSelected()) {
                    mClinicViewModel.getMutableClinics().removeObserver(allObserver);
                    if (currentLatLng == null) {
                        Toast.makeText(getContext(), "Filtering in offline mode", Toast.LENGTH_SHORT).show();
                        mClinicViewModel.getClinicsByType("Eye").observe(getViewLifecycleOwner(), new Observer<List<Clinic>>() {
                            @Override
                            public void onChanged(List<Clinic> clinics) {
                                mViewFlipper.setDisplayedChild(1);
                                mClinicAdapter.setClinics(clinics);
                                mClinicAdapter.notifyDataSetChanged();
                            }
                        });
                    } else {
                        mClinicViewModel.getClinicsByType("Eye").observe(getViewLifecycleOwner(), new Observer<List<Clinic>>() {
                            @Override
                            public void onChanged(List<Clinic> clinics) {
                                mClinicAdapter.setClinics(returnClinics(currentLatLng, clinics));
                                mClinicAdapter.notifyDataSetChanged();
                            }
                        });
                    }
                } else {
                    if (currentLatLng == null) {
                        mClinicViewModel.getMutableClinics().observe(getViewLifecycleOwner(), new Observer<List<Clinic>>() {
                            @Override
                            public void onChanged(List<Clinic> clinics) {
                                mClinicAdapter.setClinics(clinics);
                                mClinicAdapter.notifyDataSetChanged();
                            }
                        });
                    } else {
                        mClinicViewModel.getMutableClinics().observe(getViewLifecycleOwner(), allObserver);
                    }
                }
            }
        });

    }


    private void initView(View view) {
        filterCardView = view.findViewById(R.id.filterCard);
        filterTable = view.findViewById(R.id.filters);
        mRecyclerView = view.findViewById(R.id.mClinicList);
        mViewFlipper = view.findViewById(R.id.viewFlipper);
        mSwipeRefreshLayout = view.findViewById(R.id.swipeLayout);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Toast.makeText(getContext(), "LMAO IM REFRESHING", Toast.LENGTH_SHORT).show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                }, 2000);
            }
        });
        mClinicViewModel = new ViewModelProvider(requireActivity()).get(ClinicViewModel.class);
        ClinicRoomDatabase db = ClinicRoomDatabase.getDatabase(getContext());
        mClinicViewModel.setClinics(db.clinicDao().getClinics());
    }


    private List<Clinic> returnClinics(LatLng mOrigin, List<Clinic> clinics) {
        final Map<Clinic, Float> hashMap = new HashMap<>();

        for (Clinic clinic : clinics) {
            float[] results = new float[10];
            Location.distanceBetween(mOrigin.latitude, mOrigin.longitude, clinic.getLatitude(), clinic.getLongitude(), results);
            hashMap.put(clinic, results[0]);
            Log.i("Distance", clinic.getName() + results[0]);
        }

        Log.i("HashMap", hashMap.keySet().toString());

        List<Float> distances = new ArrayList<>(hashMap.values());
        Collections.sort(distances);

        List<Clinic> clinicsSorted = new ArrayList<>();

        for (int i = 0; i < distances.size(); i++) {
            Log.i("Distance", distances.get(i) + " ");
            for (Clinic clinic : hashMap.keySet()) {
                if (distances.get(i).equals(hashMap.get(clinic))) {
                    clinicsSorted.add(clinic);
                    break;
                }
            }
        }

        return clinicsSorted;
    }

    @Override
    public void scrollToTop() {

    }
}
