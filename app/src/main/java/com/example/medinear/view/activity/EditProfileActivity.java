package com.example.medinear.view.activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.medinear.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class EditProfileActivity extends AppCompatActivity {

    View view;
    ImageView backButton;
    DatabaseReference userRef;
    FirebaseUser firebaseUser;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        setContentView(R.layout.activity_edit_profile);
        loadActionBar();
        initView();

    }

    private void initView() {
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        userRef = FirebaseDatabase.getInstance().getReference().child("users").child(firebaseUser.getUid());

        final EditText nameET = findViewById(R.id.nameET);
        final EditText ephoneET = findViewById(R.id.ephoneET);
        final EditText addressET = findViewById(R.id.addressET);

        final String startName = getIntent().getStringExtra("name");
        final String startPhone = getIntent().getStringExtra("ephone");
        final String startAdd = getIntent().getStringExtra("address");

        nameET.setText(startName);
        ephoneET.setText(startPhone);
        addressET.setText(startAdd);


        final Button submitButton = findViewById(R.id.submitButton);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent infoIntent = new Intent();

                if ((nameET.getText().toString().equals(startName)) && (ephoneET.getText().toString().equals(startPhone))
                        && (addressET.getText().toString().equals(startAdd))) {
                    Toast.makeText(getApplicationContext(), "Did you make changes to the fields?",Toast.LENGTH_LONG).show();
                    return;
                } else if (TextUtils.isEmpty(nameET.getText().toString()) || TextUtils.isEmpty(ephoneET.getText().toString())
                        || TextUtils.isEmpty(addressET.getText().toString())) {
                    Toast.makeText(getApplicationContext(), "Please fill all fields",Toast.LENGTH_LONG).show();
                    return;
                }

                userRef.child("address").setValue(addressET.getText().toString());
                userRef.child("name").setValue(nameET.getText().toString());
                userRef.child("emergency_contact").setValue(ephoneET.getText().toString());

                infoIntent.putExtra("name_return",nameET.getText().toString());
                infoIntent.putExtra("ephone_return",ephoneET.getText().toString());
                infoIntent.putExtra("address_return",addressET.getText().toString());
                setResult(Activity.RESULT_OK, infoIntent);
                finish();
            }
        });
    }


    private void loadActionBar() {
        if (this.getSupportActionBar() != null) {
            this.getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setCustomView(R.layout.custom_action_bar);
            getSupportActionBar().setElevation(5);
            view = getSupportActionBar().getCustomView();
            TextView title = view.findViewById(R.id.title);
            backButton = view.findViewById(R.id.back_button);
            backButton.setOnClickListener(backListener);
            title.setText(R.string.edit_profile);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    View.OnClickListener backListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            finish();
        }
    };
}
