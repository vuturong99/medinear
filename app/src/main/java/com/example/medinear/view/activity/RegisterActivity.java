package com.example.medinear.view.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.medinear.R;
import com.example.medinear.model.User;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.auth.OAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class RegisterActivity extends AppCompatActivity {

    private static final int FACEBOOK_SIGN_IN = 3;
    LinearLayout loadingDialog;
    EditText emailET;
    EditText passwordET;
    EditText re_passwordET;
    ImageView twitterLogin;
    ImageView googleLogin;
    LoginButton facebookLogin;
    FirebaseAuth firebaseAuth;
    String name;
    FirebaseDatabase mDatabase;
    DatabaseReference mRef;
    User user;
    OAuthProvider.Builder provider;
    CallbackManager mCallbackManager;
//    ImageView facebookLogin;
//    ImageView twitterLogin;
    int RC_SIGN_IN = 1;
    private static final String TAG = "VuTST";

    GoogleSignInClient mGoogleSignInClient;

    Button registerButton;

    @Override
    protected void onStart() {
        super.onStart();
        if(firebaseAuth.getCurrentUser()!=null){
            startActivity(new Intent(getApplicationContext(), HomeActivity.class));
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        firebaseAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance();
        mRef = mDatabase.getReference();
        provider = OAuthProvider.newBuilder("twitter.com");
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this,gso);

        initViews();
        registerNormally();
        facebookSignIn();
        //


//
//        mGoogleApiClient = new GoogleApiClient.Builder(this)
//                .enableAutoManage(LoginActivity.this,this)
//                .addApi(Auth.GOOGLE_SIGN_IN_API,gso)
//                .build();
//
//        signInButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                signIn();
//            }
//        });
//
//        if(firebaseAuth.getCurrentUser()!=null){
//            startActivity(new Intent(getApplicationContext(),MainActivity.class));
//        }
//



    }

    private void registerNormally() {
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String email = emailET.getText().toString();
                String password = passwordET.getText().toString();
                String re_password = re_passwordET.getText().toString();

                if (TextUtils.isEmpty(email)) {
                    Toast.makeText(getApplicationContext(), "Please fill in your email",Toast.LENGTH_LONG).show();
                    return;
                }

                if (password.length() < 6) {
                    Toast.makeText(getApplicationContext(), "Your password must be at least 6 characters",Toast.LENGTH_LONG).show();
                    return;
                }

                if (!password.equals(re_password)) {
                    Toast.makeText(getApplicationContext(), "The passwords do not match",Toast.LENGTH_LONG).show();
                    return;
                }

                loadingDialog.setVisibility(View.VISIBLE);
                final Intent intent = new Intent(RegisterActivity.this, HomeActivity.class);

                firebaseAuth.createUserWithEmailAndPassword(email,password)
                        .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    updateUser(email);
                                    startActivity(intent);
                                    finish();
                                } else {
                                    Toast.makeText(getApplicationContext(),"Something is wrong with your email or password", Toast.LENGTH_LONG).show();
                                }

                                loadingDialog.setVisibility(View.GONE);
                            }
                        });

            }
        });

    }

    private void updateUser(String email) {
        final FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();
        user = new User("", email , "", "", "user");
        assert firebaseUser != null;
        mRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (!snapshot.exists()) {
                    mRef.child("users").child(firebaseUser.getUid()).setValue(user);
                } else {
                    Toast.makeText(RegisterActivity.this, "LUL ALREADY HAD", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
    }






    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent,RC_SIGN_IN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                GoogleSignInAccount account = task.getResult(ApiException.class);
                assert account != null;
                Log.d("VuTST","firebaseAuthWithGoogle:" + account.getId());
                name = account.getDisplayName();
                updateUser(account.getEmail());
                firebaseAuthWithGoogle(account.getIdToken());
            } catch (ApiException e) {
                Log.w("VuTST", "FAILED TO LOGIN WITH GOOGLE" + e);
            }
        }


    }

    private void firebaseAuthWithGoogle(String idToken) {
        AuthCredential credential = GoogleAuthProvider.getCredential(idToken,null);

        loadingDialog.setVisibility(View.VISIBLE);

        firebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(RegisterActivity.this, "Logged in with Google", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(RegisterActivity.this, HomeActivity.class);
                            intent.putExtra("name",name);
                            startActivity(intent);
                        } else {
                            Toast.makeText(RegisterActivity.this, "Authentication fails", Toast.LENGTH_SHORT).show();
                        }

                        loadingDialog.setVisibility(View.GONE);
                    }
                });
    }

    private void initViews() {
        loadingDialog = findViewById(R.id.loadingDialog);
        emailET = findViewById(R.id.reg_email);
        passwordET = findViewById(R.id.reg_password);
        re_passwordET = findViewById(R.id.reg_re_password);
        registerButton = findViewById(R.id.registerBtn);
        googleLogin = findViewById(R.id.googleLogin);
        twitterLogin = findViewById(R.id.twitterLogin);
        facebookLogin = findViewById(R.id.facebookLogin);

        googleLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signIn();
            }
        });
        twitterLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                twitterSignIn();
            }
        });

        emailET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b)
                    emailET.setHint("");
                else
                    emailET.setHint(R.string.email);
            }
        });

        passwordET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b)
                    passwordET.setHint("");
                else
                    passwordET.setHint(R.string.password);
            }
        });

        re_passwordET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b)
                    re_passwordET.setHint("");
                else
                    re_passwordET.setHint(R.string.retype_password);
            }
        });
    }

    private void facebookSignIn() {
        mCallbackManager = CallbackManager.Factory.create();
        facebookLogin.setPermissions("email", "public_profile");
        facebookLogin.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d(TAG, "facebook:onSuccess:" + loginResult);
                handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Log.d(TAG, "facebook:onCancel");
                loadingDialog.setVisibility(View.GONE);
                // ...
            }


            @Override
            public void onError(FacebookException error) {
                Log.d(TAG, "facebook:onError", error);
                Toast.makeText(getApplicationContext(), "Connection error", Toast.LENGTH_SHORT).show();
                loadingDialog.setVisibility(View.GONE);
            }
        });


    }
    private void twitterSignIn() {
        Task<AuthResult> pendingResultTask = firebaseAuth.getPendingAuthResult();
        if (pendingResultTask != null) {
            // There's something already here! Finish the sign-in for your user.
            pendingResultTask
                    .addOnSuccessListener(
                            new OnSuccessListener<AuthResult>() {
                                @Override
                                public void onSuccess(AuthResult authResult) {
                                    // User is signed in.
                                    // IdP data available in
                                    // authResult.getAdditionalUserInfo().getProfile().
                                    // The OAuth access token can also be retrieved:
                                    // authResult.getCredential().getAccessToken().
                                    // The OAuth secret can be retrieved by calling:
                                    // authResult.getCredential().getSecret().
                                    assert authResult.getAdditionalUserInfo() != null;
                                    name = authResult.getAdditionalUserInfo().getUsername();
                                    Toast.makeText(RegisterActivity.this, "Logged in with Twitter", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(RegisterActivity.this, HomeActivity.class);
                                    intent.putExtra("name",name);
                                    startActivity(intent);
                                }
                            })
                    .addOnFailureListener(
                            new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    // Handle failure.
                                    Toast.makeText(RegisterActivity.this, "Authentication fails", Toast.LENGTH_SHORT).show();
                                }
                            });
        } else {
            loadingDialog.setVisibility(View.VISIBLE);
            firebaseAuth
                    .startActivityForSignInWithProvider(/* activity= */ this, provider.build())
                    .addOnSuccessListener(
                            new OnSuccessListener<AuthResult>() {
                                @Override
                                public void onSuccess(AuthResult authResult) {
                                    assert authResult.getAdditionalUserInfo() != null;
                                    name = authResult.getAdditionalUserInfo().getUsername();
                                    Toast.makeText(RegisterActivity.this, "Logged in with Twitter", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(RegisterActivity.this, HomeActivity.class);
                                    intent.putExtra("name",name);
                                    loadingDialog.setVisibility(View.GONE);
                                    startActivity(intent);
                                }
                            })
                    .addOnFailureListener(
                            new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    // Handle failure.
                                    Toast.makeText(RegisterActivity.this, "Authentication failed.",
                                            Toast.LENGTH_SHORT).show();
                                    loadingDialog.setVisibility(View.GONE);
                                }
                            });

        }

    }

    private void handleFacebookAccessToken(AccessToken token) {
        Log.d(TAG, "handleFacebookAccessToken:" + token);

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        firebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = firebaseAuth.getCurrentUser();
                            Toast.makeText(RegisterActivity.this, "Logged in with Facebook",
                                    Toast.LENGTH_SHORT).show();
                            loadingDialog.setVisibility(View.GONE);
                            assert user != null;
                            updateUser(user.getEmail());
                            startActivity(new Intent(RegisterActivity.this, HomeActivity.class));
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(RegisterActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();

                        }

                        // ...
                    }
                });
    }

    public void facebookLogin(View view) {
        if (view.getId() == R.id.fbLogin) {
            facebookLogin.performClick();
            loadingDialog.setVisibility(View.VISIBLE);
        }

    }
}
