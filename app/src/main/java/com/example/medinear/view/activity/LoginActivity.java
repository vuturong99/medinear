package com.example.medinear.view.activity;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.medinear.R;
import com.example.medinear.database.ClinicRoomDatabase;
import com.example.medinear.model.Clinic;
import com.example.medinear.viewmodel.ClinicViewModel;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.OAuthProvider;
import com.google.firebase.functions.FirebaseFunctions;
import com.google.firebase.functions.FirebaseFunctionsException;
import com.google.firebase.functions.HttpsCallableResult;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    FirebaseAuth firebaseAuth;
    Button loginButton;
    EditText usernameET;
    EditText passwordET;
    Button forgotPassword;
    LinearLayout loadingDialog;
    ClinicRoomDatabase mClinicRoomDatabase;
    OAuthProvider.Builder provider;
    FirebaseFunctions mFunctions;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        firebaseAuth = FirebaseAuth.getInstance();
        mFunctions = FirebaseFunctions.getInstance("asia-east2");
        if(firebaseAuth.getCurrentUser()!=null){
            finish();
            startActivity(new Intent(getApplicationContext(), HomeActivity.class));
        }

        initViews();
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }
        });

    }



    private void login() {

        String email = usernameET.getText().toString();
        String password = passwordET.getText().toString();
        if (TextUtils.isEmpty(email)) {
            Toast.makeText(getApplicationContext(), "Please provide an email",Toast.LENGTH_LONG).show();
            return;
        }

        if (TextUtils.isEmpty(password)) {
            Toast.makeText(getApplicationContext(), "Please provide a password",Toast.LENGTH_LONG).show();
            return;
        }

        loadingDialog.setVisibility(View.VISIBLE);

        email = email.trim();
        final Intent intent = new Intent(getApplicationContext(), HomeActivity.class);


        firebaseAuth.signInWithEmailAndPassword(email,password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
//                            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
//                            if (!preferences.getBoolean("firstTime", false)) {
//                                fetch();
//                            } else {
//                                SharedPreferences.Editor editor = preferences.edit();
//                                editor.putBoolean("firstTime",true);
//                                editor.apply();
//                            }
//                            fetch();
                            finish();
                            startActivity(intent);
                        } else {
                            Toast.makeText(getApplicationContext(),"Invalid email or password", Toast.LENGTH_LONG).show();
                        }

                        loadingDialog.setVisibility(View.GONE);
                    }
                });
    }

    private void initViews() {
        mClinicRoomDatabase = ClinicRoomDatabase.getDatabase(getApplicationContext());
        loginButton = findViewById(R.id.loginButton);
        Button registerButton = findViewById(R.id.createAccountButton);
        forgotPassword = findViewById(R.id.forgotPassword);
        usernameET = findViewById(R.id.emailET);
        passwordET = findViewById(R.id.passwordET);
        loadingDialog = findViewById(R.id.loadingDialog);

        usernameET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b)
                    usernameET.setHint("");
                else
                    usernameET.setHint(R.string.emailLogin);
            }
        });

        passwordET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b)
                    passwordET.setHint("");
                else
                    passwordET.setHint(R.string.password);
            }
        });


        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });

        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, ForgotPasswordActivity.class));
            }
        });

    }


}
