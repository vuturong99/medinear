package com.example.medinear.view.adapter;

import android.content.Context;
import android.location.Location;
import android.media.Rating;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.medinear.R;
import com.example.medinear.model.Clinic;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;

public class ClinicAdapter extends RecyclerView.Adapter<ClinicAdapter.ViewHolder> {

    private List<Clinic> mClinicList;
    private ItemClickListener mItemClickListener;
    private LatLng mCurrentLocation;
    private StorageReference storageReference;
    FirebaseStorage firebaseStorage;
    private Context context;

    public ClinicAdapter(List<Clinic> mClinicList, ItemClickListener mItemClickListener, Context context) {
        this.mClinicList = mClinicList;
        this.mItemClickListener = mItemClickListener;
        this.context = context;
        firebaseStorage = FirebaseStorage.getInstance();
        storageReference = firebaseStorage.getReference();
    }

    public void setCurrentLocation(LatLng currentLocation) {
        this.mCurrentLocation = currentLocation;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.clinic_item, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final Clinic clinic = mClinicList.get(position);

        holder.clinicName.setText(clinic.getName());
        holder.clinicAddress.setText(clinic.getAddress());

        if (mCurrentLocation != null) {
            float[] results = new float[10];
            Location.distanceBetween(mCurrentLocation.latitude, mCurrentLocation.longitude, clinic.getLatitude(), clinic.getLongitude(), results);
            holder.distance.setText(String.format(Locale.getDefault(),"%.2f km", metersToKilometeres(results[0])));
        } else {
            holder.distance.setText(R.string.unknown_distance);
        }
        
        holder.clinicRating.setRating((float)clinic.getRating());




        //Get image from Firebase
        Picasso picasso = Picasso.get();
        picasso.setIndicatorsEnabled(false);
        picasso.load(clinic.getImageURL()).into(holder.clinicImage);


        timeMaster(clinic, holder);
//        if (clinic.isOpen()) {
//            holder.clinicStatus.setText(R.string.open);
//        } else {
//            holder.clinicStatus.setText(R.string.closed);
//        }
    }

    public void setClinics(List<Clinic> clinics) {
        mClinicList = clinics;
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        if (mClinicList != null)
            return mClinicList.size();
        else
            return 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView clinicName;
        TextView clinicAddress;
        ImageView clinicImage;
        TextView clinicStatus;
        MaterialRatingBar clinicRating;
        TextView distance;



        ViewHolder(@NonNull View itemView) {
            super(itemView);
            itemView.setTag(this);

            distance = itemView.findViewById(R.id.distance);
            clinicName = itemView.findViewById(R.id.clinicName);
            clinicAddress = itemView.findViewById(R.id.clinicAddress);
            clinicImage = itemView.findViewById(R.id.clinicImage);
            clinicStatus = itemView.findViewById(R.id.clinicStatus);
            clinicRating = itemView.findViewById(R.id.clinicRating);

            View.OnClickListener itemViewClick = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mItemClickListener.onClick(mClinicList.get(getAdapterPosition()));
                }
            };
            itemView.setOnClickListener(itemViewClick);

        }
    }


    public interface ItemClickListener {
        void onClick(Clinic clinic);
    }

    public Float metersToKilometeres(Float meters) {
        return meters/1000;
    }

    public void timeMaster(Clinic clinic, ViewHolder holder) {
        String getTime;
        Date now = new Date();
        Date time1;
        Date time2;
        String timeNow;

        SimpleDateFormat simpleDateformat = new SimpleDateFormat("E",Locale.US);
        SimpleDateFormat simpleTimeFormat = new SimpleDateFormat("hh:mm a",Locale.US);
        switch (simpleDateformat.format(now)) {
            case "Mon":
                getTime = clinic.getMonday_time();
                break;
            case "Tue":
                getTime = clinic.getTuesday_time();
                break;
            case "Wed":
                getTime = clinic.getWednesday_time();
                break;
            case "Thu":
                getTime = clinic.getThursday_time();
                break;
            case "Fri":
                getTime = clinic.getFriday_time();
                break;
            case "Sat":
                getTime = clinic.getSaturday_time();
                break;
            case "Sun":
                getTime = clinic.getSunday_time();
                break;
            default:
                getTime = "Open: 7:30 AM - Close: 4:30 PM";
        }

        if (getTime.contains("24")) {
            holder.clinicStatus.setTextColor(context.getResources().getColor(R.color.darkGreen));
            holder.clinicStatus.setText(R.string.open);
            clinic.setOpen(true);
        } else if (getTime.contains("Closed")) {
            holder.clinicStatus.setTextColor(context.getResources().getColor(R.color.darkRed));
            holder.clinicStatus.setText(R.string.closed);
            clinic.setOpen(false);
        } else {
            String[] timeStrings = getTime.split(" ");

            try {
                time1 = simpleTimeFormat.parse(timeStrings[1] + " " + timeStrings[2]);
                time2 = simpleTimeFormat.parse(timeStrings[timeStrings.length - 2] + " " + timeStrings[timeStrings.length - 1]);
                timeNow = simpleTimeFormat.format(new Date());
                now = simpleTimeFormat.parse(timeNow);
                Log.i("TIME",now + "");
                Log.i("TIME",timeNow);

                if (time1.before(now) && time2.after(now)) {
                    holder.clinicStatus.setTextColor(context.getResources().getColor(R.color.darkGreen));
                    holder.clinicStatus.setText(R.string.open);
                    clinic.setOpen(true);
                } else {
                    holder.clinicStatus.setTextColor(context.getResources().getColor(R.color.darkRed));
                    holder.clinicStatus.setText(R.string.closed);
                    clinic.setOpen(false);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }
}
