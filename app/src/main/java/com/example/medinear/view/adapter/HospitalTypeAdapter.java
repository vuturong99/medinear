package com.example.medinear.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.medinear.R;
import com.example.medinear.model.Clinic;
import com.example.medinear.model.HospitalType;

import java.util.List;

public class HospitalTypeAdapter extends RecyclerView.Adapter<HospitalTypeAdapter.ViewHolder> {

    List<HospitalType> hospitalTypeList;
    ItemClickListener itemClickListener;

    public HospitalTypeAdapter(List<HospitalType> hospitalTypeList, ItemClickListener itemClickListener) {
        this.hospitalTypeList = hospitalTypeList;
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.clinic_type_item, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        HospitalType hospitalType = hospitalTypeList.get(position);

        holder.hospitalType.setText(hospitalType.getDisplayedText());
        holder.checkBox.setChecked(hospitalType.isChecked());
    }

    @Override
    public int getItemCount() {
        return hospitalTypeList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView hospitalType;
        CheckBox checkBox;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            hospitalType = itemView.findViewById(R.id.hospitalType);
            checkBox = itemView.findViewById(R.id.checkBox);

            View.OnClickListener itemClick = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemClickListener.onClick(hospitalTypeList.get(getAdapterPosition()));
                }
            };

            itemView.setOnClickListener(itemClick);
        }
    }

    public interface ItemClickListener {
        void onClick(HospitalType hospitalType);
    }

}

