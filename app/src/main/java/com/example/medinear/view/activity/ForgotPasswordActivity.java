package com.example.medinear.view.activity;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.medinear.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

public class ForgotPasswordActivity extends AppCompatActivity {

    FirebaseAuth mFirebaseAuth;
    Button recoverBtn;
    EditText email;
    ImageView backButton;
    View view;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        setContentView(R.layout.activity_forgot_password);

        mFirebaseAuth = FirebaseAuth.getInstance();
        initViews();

        loadActionBar();
    }

    private void initViews() {
        recoverBtn = findViewById(R.id.recoverButton);
        email = findViewById(R.id.recoverEmail);

        email.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    email.setHint("");
                }
            }
        });

        recoverBtn.setOnClickListener(recoverListener);
    }

    private void loadActionBar() {
        if (this.getSupportActionBar() != null) {
            this.getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setCustomView(R.layout.custom_action_bar);
            getSupportActionBar().setElevation(5);
            view = getSupportActionBar().getCustomView();
            TextView title = view.findViewById(R.id.title);
            backButton = view.findViewById(R.id.back_button);
            backButton.setOnClickListener(backListener);
            title.setText(R.string.password_recovery);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    View.OnClickListener backListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            finish();
        }
    };

    View.OnClickListener recoverListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            String recoverEmail = email.getText().toString();

            if (TextUtils.isEmpty(recoverEmail)) {
                Toast.makeText(getApplicationContext(), "Please provide an email",Toast.LENGTH_LONG).show();
                return;
            }

            mFirebaseAuth.sendPasswordResetEmail(recoverEmail)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                Toast.makeText(getApplicationContext(), "Email sent", Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
        }
    };
}
