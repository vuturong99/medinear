package com.example.medinear.view.fragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.example.medinear.R;
import com.example.medinear.api.JsonPlaceHolderApi;
import com.example.medinear.api.air_quality_model.AirStatus;
import com.example.medinear.api.weather_model.Weather;
import com.example.medinear.viewmodel.AirQualityViewModel;
import com.example.medinear.viewmodel.LocationViewModel;
import com.google.android.gms.maps.model.LatLng;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class AirQualityFragment extends Fragment {

    private SwipeRefreshLayout swipeRefreshLayout;
    private LatLng currentLatLng;
    private Retrofit retrofitAQI;
    private Retrofit retrofitWeather;
    private Call<AirStatus> call;
    private TextView locationName;
    private TextView aqi;
    private TextView date;
    private TextView status;
    private ImageView face;
    private TextView pm25val;
    private TextView pm10val;
    private TextView so2val;
    private TextView no2val;
    private TextView o3val;
    private TextView coval;
    private TextView currentTemp, currentHumidity, currentWindspeed;
    private TextView day2,day3,day4,day5,day6,day2t,day3t,day4t,day5t,day6t;
    private ImageView currentWeather, day2w, day3w, day4w, day5w, day6w;
    private List<ImageView> weathers = new ArrayList<>();
    private LinearLayout aqiCard;
    private AirQualityViewModel airQualityViewModel;
    private int currentAqi = 1;
    private ArrayList<String> weatherDescription = new ArrayList();
    private ViewFlipper viewFlipper;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_air_quality, container, false);

        init();
        initViews(view);


        return view;

    }

    private void initViews(View view) {
        swipeRefreshLayout = view.findViewById(R.id.swipeLayout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                callAQIAPI();
                callWeatherAPI();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }, 2000);
            }
        });
        viewFlipper = view.findViewById(R.id.view_flipper_2);
        locationName = view.findViewById(R.id.locationName);
        date = view.findViewById(R.id.date);
        aqi = view.findViewById(R.id.aqi);
        status = view.findViewById(R.id.status);
        face = view.findViewById(R.id.face);
        o3val = view.findViewById(R.id.o3val);
        so2val = view.findViewById(R.id.so2val);
        coval = view.findViewById(R.id.coval);
        no2val = view.findViewById(R.id.no2val);
        pm25val = view.findViewById(R.id.pm2_5val);
        pm10val = view.findViewById(R.id.pm10val);
        aqiCard = view.findViewById(R.id.aqi_card);

        day2 = view.findViewById(R.id.day2);
        day3 = view.findViewById(R.id.day3);
        day4 = view.findViewById(R.id.day4);
        day5 = view.findViewById(R.id.day5);
        day6 = view.findViewById(R.id.day6);


        day2t = view.findViewById(R.id.day2t);
        day3t = view.findViewById(R.id.day3t);
        day4t = view.findViewById(R.id.day4t);
        day5t = view.findViewById(R.id.day5t);
        day6t = view.findViewById(R.id.day6t);


        day2w = view.findViewById(R.id.day2w);
        day3w = view.findViewById(R.id.day3w);
        day4w = view.findViewById(R.id.day4w);
        day5w = view.findViewById(R.id.day5w);
        day6w = view.findViewById(R.id.day6w);

        currentWeather = view.findViewById(R.id.currentWeather);
        weathers.add(currentWeather);
        weathers.add(day2w);
        weathers.add(day3w);
        weathers.add(day4w);
        weathers.add(day5w);
        weathers.add(day6w);

        currentTemp = view.findViewById(R.id.currentTemp);
        currentHumidity = view.findViewById(R.id.currentHumidity);
        currentWindspeed = view.findViewById(R.id.currentWindspeed);

        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        Date dateZ = new Date();
        date.setText(formatter.format(dateZ));

        viewFlipper.setDisplayedChild(0);
    }

    private void init() {
        airQualityViewModel = new ViewModelProvider(requireActivity()).get(AirQualityViewModel.class);
        airQualityViewModel.getAqi().observe(getViewLifecycleOwner(), new Observer<Integer>() {
            @Override
            public void onChanged(Integer aqi) {
                currentAqi = aqi;
                Log.i("AQI",currentAqi + "");
                updateUIAQI(aqi);
            }
        });

        currentLatLng = new LatLng(21,105);
        retrofitAQI = new Retrofit.Builder().baseUrl("https://api.weatherbit.io/v2.0/current/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        retrofitWeather = new Retrofit.Builder().baseUrl("https://api.weatherbit.io/v2.0/forecast/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();


    }

    private void callAQIAPI() {
        JsonPlaceHolderApi jsonPlaceHolderApi = retrofitAQI.create(JsonPlaceHolderApi.class);

        call = jsonPlaceHolderApi.getStatus(currentLatLng.latitude, currentLatLng.longitude,"7401b814fc8543f39897f910ea283a4c");
        call.enqueue(new Callback<AirStatus>() {
            @Override
            public void onResponse(Call<AirStatus> call, Response<AirStatus> response) {
                if (!response.isSuccessful()) {
                    Log.i("ERRORRRR",response.code() + "");
                    return;
                }

                AirStatus status = response.body();
                assert status != null;
                locationName.setText(status.getCity_name());
                aqi.setText(status.getData().get(0).getAqi());
                pm10val.setText(status.getData().get(0).getPm10());
                coval.setText(status.getData().get(0).getCo());
                so2val.setText(status.getData().get(0).getSo2());
                no2val.setText(status.getData().get(0).getNo2());
                pm25val.setText(status.getData().get(0).getPm2p5());
                o3val.setText(status.getData().get(0).getO3());

                updateUIAQI(Integer.valueOf(status.getData().get(0).getAqi()));
            }

            @Override
            public void onFailure(Call<AirStatus> call, Throwable t) {
                Log.i("ERROR", t.getMessage());
            }
        });
    }

    private void callWeatherAPI() {
        JsonPlaceHolderApi jsonPlaceHolderApi = retrofitWeather.create(JsonPlaceHolderApi.class);

        Call<Weather> call = jsonPlaceHolderApi.getWeather(currentLatLng.latitude, currentLatLng.longitude,
                    "7401b814fc8543f39897f910ea283a4c");

        call.enqueue(new Callback<Weather>() {
            @Override
            public void onResponse(Call<Weather> call, Response<Weather> response) {
                if (!response.isSuccessful()) {
                    Log.i("ERRORRRR",response.code() + "");
                    return;
                }

                Weather weather = response.body();

                assert weather != null;
                currentTemp.setText(String.format(Locale.getDefault(),"%.0f\u00B0",
                        Float.valueOf(weather.getData().get(0).getTemp())));

                currentHumidity.setText(String.format("%s%%", weather.getData().get(0).getRelative_humidity()));
                currentWindspeed.setText(String.format(Locale.getDefault(),"%.2f km/h",
                        Float.valueOf(weather.getData().get(0).getWind_speed())));

                formatDate(weather);

                for (int i = 0; i < 6; i++) {
                    weatherDescription.add(weather.getData().get(i).getWeatherDescriptionList().getDescription());
                }

                updateUIWeather(weatherDescription);
                viewFlipper.setDisplayedChild(1);
            }

            @Override
            public void onFailure(Call<Weather> call, Throwable t) {
                Log.i("ERRORRRR",t.getMessage() + "");
            }
        });
    }

    private void formatDate(Weather weather) {
        day2t.setText(weather.getData().get(1).getTemp());
        day3t.setText(weather.getData().get(2).getTemp());
        day4t.setText(weather.getData().get(3).getTemp());
        day5t.setText(weather.getData().get(4).getTemp());
        day6t.setText(weather.getData().get(5).getTemp());


        Date date2 = new Date();
        Date date3 = new Date();
        Date date4 = new Date();
        Date date5 = new Date();
        Date date6 = new Date();
        try {
            date2= new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).parse(weather.getData().get(1).getDatetime());
            date3= new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).parse(weather.getData().get(2).getDatetime());
            date4= new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).parse(weather.getData().get(3).getDatetime());
            date5= new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).parse(weather.getData().get(4).getDatetime());
            date6= new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).parse(weather.getData().get(5).getDatetime());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat simpleDateformat = new SimpleDateFormat("dd/MM", Locale.US);


        assert date2 != null;
        day2.setText(simpleDateformat.format(date2));
        assert date3 != null;
        day3.setText(simpleDateformat.format(date3));
        assert date4 != null;
        day4.setText(simpleDateformat.format(date4));
        assert date5 != null;
        day5.setText(simpleDateformat.format(date5));
        assert date6 != null;
        day6.setText(simpleDateformat.format(date6));

    }

    private void updateUIWeather(ArrayList<String> weatherDescription) {
        for (int i = 0; i < 6; i++) {
            Log.i("WEATHER", i + "");
            Log.i("WEATHER",weatherDescription.get(i) + "");
            if(( weatherDescription.get(i).contains("clouds") || weatherDescription.get(i).contains("Clouds"))) {
                weathers.get(i).setImageDrawable(getResources().getDrawable(R.drawable.cloudy));
            } else if ((weatherDescription.get(i).contains("Thunderstorm"))) {
                weathers.get(i).setImageDrawable(getResources().getDrawable(R.drawable.storm));
            } else if ((weatherDescription.get(i).contains("rain") || weatherDescription.get(i).contains("Rain"))) {
                weathers.get(i).setImageDrawable(getResources().getDrawable(R.drawable.storm));
            }
        }
    }

    private void updateUIAQI(int aqi) {
        if ((0 <= aqi) && (aqi <= 50)) {
            aqiCard.setBackgroundResource(R.drawable.background_round_corner_green1);
            face.setBackgroundResource(R.drawable.background_round_corner_green2);
            face.setImageDrawable(getResources().getDrawable(R.drawable.ic_good_aqi));
            status.setText(R.string.good_aqi);
        } else if ((51 <= aqi) && (aqi <= 100)) {
            aqiCard.setBackgroundResource(R.drawable.background_round_corner_yellow1);
            face.setBackgroundResource(R.drawable.background_round_corner_yellow2);
            face.setImageDrawable(getResources().getDrawable(R.drawable.ic_normal_aqi));
            status.setText(R.string.moderate_aqi);
        } else if ((101 <= aqi) && (aqi <= 150)) {
            aqiCard.setBackgroundResource(R.drawable.background_round_corner_orange1);
            face.setBackgroundResource(R.drawable.background_round_corner_orange2);
            face.setImageDrawable(getResources().getDrawable(R.drawable.ic_almost_bad_aqi));
            status.setText(R.string.unhealthy_fsg_aqi);
        } else if ((151 <= aqi) && (aqi <= 200)) {
            aqiCard.setBackgroundResource(R.drawable.background_round_corner_red1);
            face.setBackgroundResource(R.drawable.background_round_corner_red2);
            face.setImageDrawable(getResources().getDrawable(R.drawable.ic_bad_aqi));
            status.setText(R.string.unhealthy_aqi);
        } else if ((201 <= aqi) && (aqi <= 300)) {
            aqiCard.setBackgroundResource(R.drawable.background_round_corner_purple1);
            face.setBackgroundResource(R.drawable.background_round_corner_purple2);
            face.setImageDrawable(getResources().getDrawable(R.drawable.ic_very_bad_aqi));
            status.setText(R.string.very_unh_aqi);
        } else if ((301 <= aqi) && (aqi <= 500)) {
            aqiCard.setBackgroundResource(R.drawable.background_round_corner_brown1);
            face.setBackgroundResource(R.drawable.background_round_corner_brown2);
            face.setImageDrawable(getResources().getDrawable(R.drawable.ic_extremely_bad_aqi));
            status.setText(R.string.hazardous_aqi);
        }
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final LocationViewModel viewModel = new ViewModelProvider(requireActivity()).get(LocationViewModel.class);
        viewModel.getCurrentLocation().observe(getViewLifecycleOwner(), new Observer<LatLng>() {
            @Override
            public void onChanged(LatLng latLng) {
                currentLatLng = latLng;
                callAQIAPI();
                callWeatherAPI();
                viewModel.getCurrentLocation().removeObserver(this);
            }
        });

    }
}
