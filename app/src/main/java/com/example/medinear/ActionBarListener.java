package com.example.medinear;

public interface ActionBarListener {
    void onClickListener(String text);

    void onSearchListener(String text);
}
