var admin = require("firebase-admin");

var serviceAccount = require("./medinear-7f7c1-firebase-adminsdk-7w5s2-22bd7261f2.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://medinear-7f7c1.firebaseio.com"
});

const functions = require('firebase-functions');
const db = admin.database();
const db2 = admin.firestore();
const express = require('express');
const cors = require('cors');
const app = express();
app.use(cors({ origin: true }));

app.get('/hello-world', (req, res) => {
  return res.status(200).send('Hello World!');
});

exports.getAllHospitals = functions.region("asia-east2").https.onCall((data, context) => {

  if (!context.auth) {
    // Throwing an HttpsError so that the client gets the error details.
    throw new functions.https.HttpsError('failed-precondition', 'The function must be called ' +
        'while authenticated.');
  }
  
  // //Get role here
  var ref = db.ref("hospitals")

  // Attach an asynchronous callback to read the data at our posts reference
  ref.on("value", (snapshot) => {
    console.log(snapshot.val());
  }, (errorObject) => {
    console.log("The read failed: " + errorObject.code);
  });

  return db.ref("/hospitals").once('value')   // use once() here
    .then(snapshot => {
        return snapshot.val()
    })
    .catch((error) => {
        throw new functions.https.HttpsError('unknown', error.message, error);
    });
});

exports.rate = functions.region("asia-east2").https.onCall((data,context) =>{
   if (!context.auth) {
    // Throwing an HttpsError so that the client gets the error details.
    throw new functions.https.HttpsError('failed-precondition', 'The function must be called ' +
        'while authenticated.');
  }

  const id = date.id;
  const rate = data.rate;
  return db.ref("/clinics/" + id).push({
    rating: rate
  }).then(() => {
    console.log("Rated");
    return {rating: rate};
  });
})

exports.getOpeningHours = functions.region("asia-east2").https.onCall((data, context) => {

  if (!context.auth) {
    // Throwing an HttpsError so that the client gets the error details.
    throw new functions.https.HttpsError('failed-precondition', 'The function must be called ' +
        'while authenticated.');
  }
  
  // // //Get role here
  // var ref = db.ref("opening_hours")

  // // Attach an asynchronous callback to read the data at our posts reference
  // ref.on("value", (snapshot) => {
  //   console.log(snapshot.val());
  // }, (errorObject) => {
  //   console.log("The read failed: " + errorObject.code);
  // });

  return db.ref("/opening_hours").once('value')   // use once() here
    .then(snapshot => {
        return snapshot.val()
    })
    .catch((error) => {
        throw new functions.https.HttpsError('unknown', error.message, error);
    });
});
exports.addNumbers = functions.https.onCall((data) => {
    // [END addFunctionTrigger]
      // [START readAddData]
      // Numbers passed from the client.
      const firstNumber = data.firstNumber;
      const secondNumber = data.secondNumber;
      // [END readAddData]
    
      // [START addHttpsError]
      // Checking that attributes are present and are numbers.
      if (!Number.isFinite(firstNumber) || !Number.isFinite(secondNumber)) {
        // Throwing an HttpsError so that the client gets the error details.
        throw new functions.https.HttpsError('invalid-argument', 'The function must be called with ' +
            'two arguments "firstNumber" and "secondNumber" which must both be numbers.');
      }
      // [END addHttpsError]
    
      // [START returnAddData]
      // returning result.
      return {
        firstNumber: firstNumber,
        secondNumber: secondNumber,
        operator: '+',
        operationResult: firstNumber + secondNumber,
      };
      // [END returnAddData]
    });
    // [END allAdd]
// const validateFirebaseIdToken = async (req, res, next) => {
//     idToken = req.headers.authorization.split('Bearer ')[1];
//     try {
//         const decodedIdToken = await admin.auth().verifyIdToken(idToken);
//         console.log('ID Token correctly decoded', decodedIdToken);
//         req.user = decodedIdToken;
//         next();
//         return;
//     } catch (error) {
//         console.error('Error while verifying Firebase ID token:', error);
//         res.status(403).send('Unauthorized');
//         return;
//     }
//   };

// app.use(validateFirebaseIdToken)
// // create
// app.post('/api/create', (req, res) => {
//     (async () => {
//         try {
//           await db.collection('items').doc('/' + req.body.id + '/')
//               .create({item: req.body.item});
//           return res.status(200).send();
//         } catch (error) {
//           console.log(error);
//           return res.status(500).send(error);
//         }
//       })();
//   });

//   // read item
// app.get('/api/read/:item_id', (req, res) => {
//     (async () => {
//         try {
//             const document = db.collection('items').doc(req.params.item_id);
//             let item = await document.get();
//             let response = item.data();
//             return res.status(200).send(response);
//         } catch (error) {
//             console.log(error);
//             return res.status(500).send(error);
//         }
//         })();
//     });

// read all
app.get('/api/read', (req, res) => {
    (async () => {
        try {
            let query = db2.collection('items');
            let response = [];
            await query.get().then(querySnapshot => {
            let docs = querySnapshot.docs;
            // eslint-disable-next-line promise/always-return
            for (let doc of docs) {
                const selectedItem = {
                    id: doc.id,
                    item: doc.data().item
                };
                response.push(selectedItem);
            }
            });
            return res.status(200).send(response);
        } catch (error) {
            console.log(error);
            return res.status(500).send(error);
        }
        })();
    });

// // update
// app.put('/api/update/:item_id', (req, res) => {
// (async () => {
//     try {
//         const document = db.collection('items').doc(req.params.item_id);
//         await document.update({
//             item: req.body.item
//         });
//         return res.status(200).send();
//     } catch (error) {
//         console.log(error);
//         return res.status(500).send(error);
//     }
//     })();
// });

// // delete
// app.delete('/api/delete/:item_id', (req, res) => {
// (async () => {
//     try {
//         const document = db.collection('items').doc(req.params.item_id);
//         await document.delete();
//         return res.status(200).send();
//     } catch (error) {
//         console.log(error);
//         return res.status(500).send(error);
//     }
//     })();
// });

// app.get('/api/read/:item_id', (req, res) => {
//     (async () => {
//         try {
//             const document = db.collection('items').doc(req.params.item_id);
//             let item = await document.get();
//             let response = item.data();
//             return res.status(200).send(response);
//         } catch (error) {
//             console.log(error);
//             return res.status(500).send(error);
//         }
//         })();
//     });

exports.addMessage = functions.https.onCall(async (data, context) => {
    // [START_EXCLUDE]
    // [START readMessageData]
    // Message text passed from the client.
    const text = data.text;
    // [END readMessageData]
    // [START messageHttpsErrors]
    // Checking attribute.
    if (!(typeof text === 'string') || text.length === 0) {
      // Throwing an HttpsError so that the client gets the error details.
      throw new functions.https.HttpsError('invalid-argument', 'The function must be called with ' +
          'one arguments "text" containing the message text to add.');
    }
    // Checking that the user is authenticated.
    if (!context.auth) {
      // Throwing an HttpsError so that the client gets the error details.
      throw new functions.https.HttpsError('failed-precondition', 'The function must be called ' +
          'while authenticated.');
    }
    // [END messageHttpsErrors]
  
    // [START authIntegration]
    // Authentication / user information is automatically added to the request.
    const uid = context.auth.uid;
    const name = context.auth.token.name || null;
    const picture = context.auth.token.picture || null;
    const email = context.auth.token.email || null;
    // [END authIntegration]
  
    // [START returnMessageAsync]
    // Saving the new message to the Realtime Database.
    try {
    await admin.database().ref('/messages').push({
      text: text,
      author: { uid, name, picture, email },
    });
    console.log('New Message written');
    return { text: text };
  }
  catch (error) {
    // Re-throwing the error as an HttpsError so that the client gets the error details.
    throw new functions.https.HttpsError('unknown', error.message, error);
  }
    // [END_EXCLUDE]
  });
  // [END messageFunctionTrigger]


exports.app = functions.region("asia-east2").https.onRequest(app)

