import scrapy
from vvscraper.items import ClinicItem
from vvscraper.items import ClinicItem2
from scrapy.loader import ItemLoader

class ClinicScraper(scrapy.Spider):
    name = "clinic_spider"
    start_urls = ["https://suckhoe2t.net/danh-sach-cac-benh-vien-tai-tp-ha-noi/"]

    def parse(self, response):
        # for clinic in response.xpath("//ul[@class='toc_list']/li/ul/li"):
        #     l = ItemLoader(item = ClinicItem(), selector=clinic)
        #     l.add_xpath('clinic_text', ".//a")
        #     l.add_xpath('clinic_lol', ".//a")
        #     yield l.load_item() 
        
        # for clinic in response.xpath("//div[@class='td-post-content']/table/tbody/tr/td/ul"):
        #     l = ItemLoader(item = ClinicItem(), selector=clinic)
        #     l.add_xpath('address', ".//li")
        #     yield l.load_item()
        
        for clinic in response.xpath("//div[@class='review-box']"):
            l = ItemLoader(item = ClinicItem2(), selector=clinic)
            l.add_xpath('rating',".//div[@class='review-number']")
            yield l.load_item()
        