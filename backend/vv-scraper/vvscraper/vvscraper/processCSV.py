import csv

clinicNames = []
addresses = []
phones = []

final_names = []
final_address = []
final_phones = []

with open("data.csv","r",encoding="utf-8") as fi:
    reader = csv.reader(fi)
    for row in reader:
        row = "".join(row)
        clinicNames.append(row)
    

    for row in clinicNames:
        row = row.replace("–","")
        row = row.replace("  ", " ")

        row = row.split(" ", 1)[1]
        rowsplit = row.split(" ")
        for word in rowsplit:
            if word == "TP" or word == "-":
                rowsplit.remove(word)
        result = ' '.join(rowsplit)
        result = result.split('  ')[0].split("DANH ")[0].strip()
   
        final_names.append(result)



with open("test.csv","r", encoding="utf-8") as fi:
    reader = csv.reader(fi)

    stopwords = ["Phố","Đường", "phường", "Quận", "P.", "Tp.", "phố","TP.","-", "thành"]
    for row in reader:
        row = "".join(row)
        addresses.append(row)
       
    for row in addresses:
        row = ' '.join(row.split()[2:])
        rowsplit = row.split()
        result = [word for word in rowsplit if word not in stopwords]
        row = ' '.join(result)
        final_address.append(row)

with open("test2.csv","r", encoding="utf-8") as fi:
    reader = csv.reader(fi)

    for row in reader:
        row = "".join(row)
        phones.append(row)
    
    for row in phones:
        row = ' '.join(row.split()[2:])
        row = row.split("– ")[0].strip()
        final_phones.append(row)



with open("clinic.csv","w", encoding="utf-8", newline = '') as fo:
    writer = csv.writer(fo)
    for name in final_names:
        writer.writerow([name])

with open("address.csv","w", encoding="utf-8", newline = '') as fo:
    writer = csv.writer(fo, quoting = csv.QUOTE_ALL)
    for i in range(len(final_address)):
        print(final_names[i])
        writer.writerow([final_names[i]] + [final_address[i]] + [final_phones[i]])