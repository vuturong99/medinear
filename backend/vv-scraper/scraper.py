import scrapy


class ClinicScraper(scrapy.Spider):
    name = "clinic_spider"
    start_urls = ["https://suckhoe2t.net/danh-sach-cac-benh-vien-tai-tp-ha-noi/"]

    def parse(self, response):
        for clinic in response.xpath("//div[@class='td-post-content']/table/tbody/tr/td/h3/span"):
            yield {
                "clinic name" : clinic.xpath(".//strong").extract_first()
            }